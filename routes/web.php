<?php
use App\Projet;
use App\Paiement;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route vers la page d'Accueil ou Login

Route::get('/', function () {
    $page = null;
    if(Auth::check()){
        $user = Auth::user();
        $projets = Projet::all();
        $paiements = Paiement::all();
        $page = view('home', compact('projets', 'paiements', 'user'));
    }
    else{
        $page = view('auth/login');
    }
    return $page;
});


// ! Routes accessibles par tous
Auth::routes(['register' => false]);
Route::get('/affilie/edit', 'AffilieController@editUser');
Route::get('/facture/{idProjet}', 'FactureController@validateAuth');
Route::get('/soumission/{lien}', 'FormulaireController@showForm');
Route::post('/reponse', 'ReponseController@store');
Route::get('/resultat/{id}', 'ReponseController@show');
Route::get('/paiement/show', 'PaiementController@showUser');
Route::patch('/affilie/updateUser/{id}', 'AffilieController@updateUser');

// !Routes accessibles par l'administrateur seulement
Route::middleware(['admin'])->group(function(){
    Route::resource('note', 'NoteController');
    Route::post("/formulaire/updateActive", 'FormulaireController@updateActive');

    Route::resource('affilie', 'AffilieController');
    Route::resource('projet', 'ProjetController');
    Route::resource('paiement', 'PaiementController');
    Route::resource('formulaire', 'FormulaireController');
    Route::resource('question', 'QuestionController');

    Route::post('/question/afficher_question', 'QuestionController@afficher_question');
    Route::post('/question/afficher_section', 'QuestionController@afficher_section');
    Route::post('/question/{id}/delete', 'QuestionController@destroy');

    Route::post('/etape/add', 'EtapeController@store');
    Route::post('/etape/{etapeid}/delete', 'EtapeController@destroy');

    Route::get('/note/create/{idAffilie}', 'NoteController@create');
    Route::post('/note/{idAffilie}', 'NoteController@store');
    Route::post('/note/{noteID}/update', "NoteController@update");
    Route::post('/note/{noteID}/delete', "NoteController@destroy");
    Route::post('/note/add', "NoteController@store");

});
