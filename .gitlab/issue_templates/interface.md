<!---
Gabarit pour les erreurs d'interfaces ou les problèmes d'affichage
--->

### Résumé du problème

(Résumé ce qui ne fonctionne pas dans l'interface)

### Dans quelle page est-ce que le problème survient ?

(Dans quelle section du site web êtes vous lorsque le problème survient.)

### Impression d'écran

(Fournir une impression d'écran pour faciliter la compréhension du problème.)

### Correctifs possibles

(Selon vous, comment pourrions-nous améliorer la situation )


/label ~Interface