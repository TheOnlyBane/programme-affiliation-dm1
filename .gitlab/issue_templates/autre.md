<!--
Gabarit pour la gestion de problèmes autres
-->

### Résumé du problème

(Résumé précisément en quoi consiste le problème)

### Étapes à reproduire

(Comment reproduire le problème)

### Où se situe ce problème (page) (Facultatif)

(Dans quelle page peut-on retrouver le problème)

/label ~Autre