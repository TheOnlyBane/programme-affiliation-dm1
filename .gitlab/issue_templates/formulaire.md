<!---
Erreur présent dans le formulaire
--->

### Résumé du problème

(Résumé précisément en quoi consiste le problème)

### Étape à reproduire

(Comment peut-on reproduire le problème)

### Quel est le comportement "erroné" actuel ? 

(Qu'est-ce qui se passe et qui n'est pas correcte ?)

### Quel est le comportement "correct" attendu ? 

(Que devrait-il se passer au lieu de se qui se passe présentement?)

### Correctifs possibles

(Si vous pensez avoir une piste de solution ou une suggestion pour diriger 
la correction du problème, écrivez la ici.)


/label ~Formulaire