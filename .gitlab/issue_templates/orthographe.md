<---
Gabarit de correction de fautes d'orthographe ou structure de phrase
--->

### Indiquer où il y a une faute d'orthographe ou le problème de structure

(Indiquer sur quelle page il y a une faute d'orthographe)

### Inscrire la phrase qui contient le problème

(Inscrire ici la phrase avec la faute d'orthographe)

### Correction(s) possible(s)

(Comment corrigeriez vous le problème)


/label ~Orthographe