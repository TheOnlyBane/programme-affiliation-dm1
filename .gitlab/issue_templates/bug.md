<!---
Gabarit pour les "bugs" de fonctionnalités
--->

### Résumé du problème

(Résumé précisément en quoi consiste le problème)

### Étapes à reproduire

(Comment reproduire le problème)

### En quoi le comportement est-il erroné ?

(Qu'est-ce qui se passe qui ne devrait pas être là)

### Quel est le comportement voulu ?

(Que devrait-il se passer au lieu de l'erreur)

### Impressions d'écrans ou journal

(Ajouter des informations pertinentes permettant de clarifier le problème encontré)

### Correctifs possibles

(Si vous pensez avoir une piste de solution ou une suggestion
pour diriger la correction du problème, écrivez la ici.)

/label ~Bug