/**
 * Permet d'afficher le contenu de l'étape clicker
 */
function evenement_activer(){
    var etapeid = jQuery(this).parent().data('etapeid');
    if(!jQuery(this).parent().hasClass('active')) {
        jQuery('.link-etape').parent().removeClass('active');
        jQuery(this).parent().addClass('active');
        jQuery('.contenu-etape').removeClass('active');
        jQuery('.contenu-etape[data-etapeid="'+etapeid+'"]')
            .addClass('active');
    }
}

/**
 * Permet de supprimer une etape
 */
function supprimer_etape(){
    var etapeid = jQuery(this).parent().data('etapeid');
    var a_supprimer = jQuery(this).parent();
    var nbr_etape = jQuery(this).parent().data('etapenbr');

    var nombre_question = jQuery('.contenu-etape[data-etapeid="'+etapeid+'"] '+
        '.wrapper-question').length;

    jQuery('.contenu-etape[data-etapeid="'+etapeid+'"] .wrapper-question')
        .each(function() {

        if (jQuery(this).data('questionid') == 'new') {
            jQuery(this).parents('.wrapper-question').remove();
            var dernier_id = jQuery('.dernier_id').val();
            jQuery('.dernier_id').val(parseInt(dernier_id)-1);
        }

        var nombre_question = jQuery('.nom_question_final').val();
        jQuery('.nom_question_final').val(parseInt(nombre_question)-1);
    });

    a_supprimer.hide().append('<input type="hidden" ' +
        'name="etape_suprimer'+nbr_etape+'" value="'+etapeid+'">');
    jQuery('.contenu-etape[data-etapeid="'+etapeid+'"]').hide();
}

/**
 * Permet d'ajouter des sections aux questions
 */
function ajouter_champs(){
    section_id = jQuery('.section_id').val();
    section_id = parseInt(section_id) + 1;
    jQuery('.section_id').val(section_id);
    var id_question = jQuery(this).parent().data('questionid');
    var nbr_choix = jQuery('.reponse-multiple'+id_question).length;

    nbr_choix = nbr_choix + 1;
    jQuery('.nbr_reponse'+id_question).val(nbr_choix);
    jQuery(this).parent().children('table').append(' <tr>\n' +
        '<td><input class="reponse-multiple'+id_question+' selection" ' +
        'name="reponse_multiple'+nbr_choix+id_question+'" type="text"></td>\n'+
        '<td><input class="montant-ajouter" type="number" ' +
        'name="montant_ajouter'+nbr_choix+id_question+'"></td>\n' +
        '<td><a href="javascript:void(0)" ' +
        'class="reponse'+nbr_choix+id_question+' supp_reponse new" ' +
        'onclick="supprimer_reponse('+nbr_choix+id_question+')">-</a></td>' +
        '<input type="hidden" class="section-id" ' +
        'name="section-id'+nbr_choix+id_question+'" value="'+section_id+'">' +
        '</tr>')

    jQuery(this).prev('.nbr_reponse').val(nbr_choix);
}

/**
 * Supprime la question selectionné
 *
 * @param nbr_reponse nombre de section dans la question
 */
function supprimer_reponse(nbr_reponse) {
    section_id = jQuery('.section_id').val();
    section_id = parseInt(section_id) - 1;
    jQuery('.section_id').val(section_id);

    jQuery('.reponse'+nbr_reponse).parents('tr').hide()
        .append('<input type="hidden" ' +
        'name="supprimer-reponse'+nbr_reponse+'" ' +
            'value="supprimer'+nbr_reponse+'">');

    if(jQuery('.reponse'+nbr_reponse).hasClass('new')) {
        var selecteur = jQuery('.reponse'+nbr_reponse)
            .parents('.wrapper-question').children('.reponse_info');
        var reponse = selecteur.val();
        selecteur.val(parseInt(reponse)-1);
    }
}

function afficher_choix(question_id, selecteur) {
    jQuery(selecteur).html('');
    var i = 1;
    jQuery('.wrapper-question[data-questionid="' + question_id + '"] ' +
        '.selection').each(function () {
        var valeur = jQuery(this).val();
        jQuery(selecteur).append(jQuery('<option>', {
            value: i,
            text: valeur
        }));
        i++;
    });
}

/**
 * Affiche l'etape et le contenu de l'etape selectionné
 * @param id de l'étape
 */
function form_afficher_etape(id){
    var i= 1;
    var objet;
    var nombreEtapes = jQuery("#nombreEtapeFormulaire").data("nombreetape");
    for (i=1; i <= nombreEtapes; i++){
        objet = jQuery("#etape"+i);
        if (id == i){
            objet.removeClass("hidden");
        }
        else {
            objet.addClass("hidden");
        }
    }
}

/**
 * Affiche les questions du formulaire
 */
function form_afficher_question(){
    var i = 1;
    var hasChanged = false;
    var nombreQuestion = jQuery("#nombreQuestionFormulaire")
        .data("nombrequestion");
    for(i =1; i <= nombreQuestion; i++){
        if (jQuery("#question"+i).length){
            var parent = jQuery('#question'+i).data("parent");
            var section = jQuery('#question'+i).data("section");
            var questionParent = jQuery("#question"+parent);
            var inputParent = jQuery("#question"+parent+"-"+section);
            if (parent != "" && section != ""){
                if (!questionParent.hasClass("hidden") &&
                    (inputParent.prop("checked") ||
                    jQuery("input[name='question"+parent+"']:checked")
                        === inputParent)){
                    hasChanged = showClass(jQuery("#question"+i), hasChanged);
                }
                else{
                    hasChanged = hideClass(jQuery("#question"+i), hasChanged);
                }
            }
            else{
                hasChanged = showClass(jQuery("#question"+i), hasChanged);
            }
        }
    }
    if (hasChanged){
        form_afficher_question();
    }
}

/**
 * Enleve la class hidden pour afficher la question
 *
 * @param question la question à afficher
 * @param hasChanged
 * @returns retour vrai si la question a changé
 */
function showClass(question, hasChanged){
    retour = hasChanged;
    if ( question.hasClass("hidden")){
        question.removeClass("hidden");
        retour = true;
    }
    return retour;
}

/**
 * Ajoute la class hidden pour cacher la question
 *
 * @param question la question à cacher
 * @param hasChanged
 * @returns retour vrai si la question a changé
 */
function hideClass(question, hasChanged){
    retour = hasChanged;
    if ( !question.hasClass("hidden")){
        question.addClass("hidden");
        retour = true;
    }
    return retour;
}

/**
 * Verfifie la progression du formulaire
 *
 * @param id du formulaire utilisé
 */
function form_progress(id){
    var nombreEtape = jQuery("#nombreEtapeFormulaire").data("nombreetape");
    var value = id/nombreEtape *100;
    jQuery("#progressBar").css("width", value+'%');
    jQuery("#progressStatus").text(Math.round(value)+"%");
}

/**
 * Affiche les conditions disponible pour la question
 */
function condition_reponse(){
    var selecteur = jQuery(this).parent().children('.wrapper-condition')
        .children('.afficher_question');
    var nom_question = jQuery(this).parents('.wrapper-question')
        .children('.nom_question.multiple').val();
    var etape_id = jQuery(this).parent().data('etapeid');

    selecteur.children('option').remove();
    selecteur.append('<option value=""></option>');

    jQuery('.contenu-etape[data-etapeid="'+etape_id+'"] ' +
        '.nom_question.multiple').each(function(){
        if(nom_question != jQuery(this).val()) {
            var question_id = jQuery(this).parents('.wrapper-question')
                .data('questionid');
            selecteur.append('<option value="'+question_id+'">'+
                jQuery(this).val()+'</option>');
        }
    });

    if (jQuery(this).parent().children('.wrapper-condition')
        .hasClass('active')) {
        jQuery(this).parent().children('.wrapper-condition')
            .removeClass('active');
    } else {
        jQuery(this).parent().children('.wrapper-condition')
            .addClass('active');
    }
    jQuery('.afficher_question option:not(.bound)').addClass('bound')
        .on('click',  ajout_reponse_condition);
}

/**
 * Ajoutes les réponses disponibles pour la question selectionné
 */
function ajout_reponse_condition(){

    var selecteur = jQuery(this).parents('.affichersi')
        .find('.afficher_reponse');

    var question_id = jQuery(this).val();
    selecteur.children('option').remove();
    selecteur.append('<option value=""></option>');
    jQuery('.wrapper-question[data-questionid="'+question_id+'"] .selection')
        .each(function(){
        var valeur = jQuery(this).val();
        var section_id = jQuery(this).parents('tr').find('.section-id').val();
        selecteur.append('<option value="'+section_id+'">'+valeur+'</option>');
    });
}

/**
 * Supprime la question selectionné
 */
function supprimer_question(){
    var nbr_section = jQuery(this).parent().find('.montant-ajouter').length;
    var section_id = jQuery('.section_id').val();
    section_id = parseInt(section_id) - parseInt(nbr_section);
    jQuery('.section_id').val(section_id);

    var selecteur = jQuery('.wrapper-question[data-questionid="'+jQuery(this)
        .data('questionid')+'"]');
    selecteur.hide()
    selecteur.append('<input type="hidden" name="a_supprimer'+jQuery(this)
            .data('questionnbr')+'" ' +
        'value="detruire">');

    if (jQuery(this).data('questionid') == 'new') {
        jQuery(this).parents('.wrapper-question').remove();
        var dernier_id = jQuery('.dernier_id').val();
        jQuery('.dernier_id').val(parseInt(dernier_id)-1);
        var nombre_question = jQuery('.nom_question_final').val();
        jQuery('.nom_question_final').val(parseInt(nombre_question)-1);
    }
}

/**
 * Ajoute une question
 */
function ajouter_question() {
    var type_question = jQuery(this).attr('id');
    var etape_id = jQuery('.etape-menu .active').data('etapeid');
    var question_id = jQuery('.wrapper-question:last-child')
        .data('questionid');
    var nbr_question = jQuery('.wrapper-question').length;
    var section_id = jQuery('.section_id').val();
    var dernier_id = jQuery('.dernier_id').val();


    nbr_question = nbr_question +1;
    if (question_id == null) {
        question_id = 1;
    } else {
        question_id = question_id + 1;
    }
    jQuery.ajax({
        type : 'POST',
        url : '/question/afficher_question',
        data: {
            'type_question' : type_question,
            'etape_id' : etape_id,
            'question_id' : question_id,
            'nbr_question' : nbr_question,
            'section_id' : section_id,
            'dernier_id' : dernier_id
        },
        success:function (data) {
            jQuery('.contenu-etape[data-etapeid="'+etape_id+'"]').append(data);
            jQuery('.supprimer_question:not(.bound)').addClass('bound')
                .on("click", supprimer_question);
            jQuery('.condition:not(.bound)').addClass('bound')
                .on('click',  condition_reponse);
            jQuery('#ajouter_champs:not(.bound)').addClass('bound')
                .on('click',  ajouter_champs);
            jQuery('.nom_question_final').val(nbr_question);
            jQuery('.dernier_id').val(parseInt(dernier_id)+1);
            if ((type_question == 'checkbox') ||
                (type_question == 'bouton_radio')) {
                jQuery('.section_id').val(parseInt(section_id)+1);
            }
        },
        error:function(jqXHR, textStatus, errorThrown){ 
            //console.log(jqXHR); 
        }
    });
}

/**
 * Modifie une note
 */
function modifier_note() {
    var noteID = jQuery(this).data("noteid");
    var dt = new Date();
    var currentDate = dt.getFullYear()+"-"+(dt.getMonth()+1)+"-"+dt.getDate();
    jQuery(this).on("focusout", function(e){
        var description = jQuery(this).val();
        jQuery.ajax({
            type:'POST',
            url:'/note/'+noteID+'/update',
            data: {
                "_token": jQuery('meta[name="csfr-token"]').attr('content'),
                "description":description,
                "date": currentDate
            },
            success:function(data){
            },
            error:function(jqXHR, textStatus, errorThrown){
                alert(textStatus);
                alert(errorThrown);
            }
        });
    })
}

/**
 * Supprime une note
 */
function supprimer_note() {
    var noteID = jQuery(this).data("noteid");
    var id = '#note'+noteID;
    jQuery.ajax({
        type:'POST',
        url:'/note/'+noteID+'/delete',
        data:{
            "_token": jQuery('meta[name="csfr-token"]').attr('content'),
            "noteID": noteID
        },
        success:function(data){
            jQuery(id).hide();
        },
        error:function(jqXHR, textStatus, errorThrown){
            //console.log(jqXHR);
        }
    });
}

/**
 * Ajoute une note
 */
function ajouter_note() {
    var affilie_id = jQuery(this).data('affilie_id');
    var description = jQuery('#descriptionNouvelleNote').val();
    if (description != null){
        jQuery.ajax({
            type:'POST',
            url:'/note/add',
            data:{
                "_token": jQuery('meta[name="csfr-token"]').attr('content'),
                "description": description,
                "affilieid": affilie_id
            },
            success:function(data){
                jQuery('#TableNote').append(data);
            },
            error:function(jqXHR, textStatus, errorThrown){
                //console.log(jqXHR);
            }
        });
    }
}

/**
 * Permet d'acceder à l'étape suivante du formulaire
 */
function etape_suivante() {
    var id = jQuery(this).data("id");
    if (jQuery("#etape"+(id+1)).length){
        form_afficher_etape(id+1);
        form_progress(id);
    }
}

/**
 * Permet d'accéder à l'étape précédente du formulaire
 */
function etape_precedente() {
    var id = jQuery(this).data("id");
    if (jQuery("#etape"+(id-1)).length){
        form_afficher_etape(id-1);
        form_progress(id-2);
    }
}

/**
 * Ajoute une étape au formulaire
 */
function ajouter_etape() {

    var formulaireID = jQuery('.etape-menu').data('formulaireid');

    var nbr_etape = jQuery('.etape-menu li:last-child').data('etapenbr');

    if (!nbr_etape) {
        nbr_etape = 0
    }

    jQuery.ajax({
        type : 'POST',
        url: '/etape/add',
        data: {
            "formulaireID" : formulaireID,
            "nbr_etape" : nbr_etape
        },
        success:function(data) {
            jQuery('.nbr_etape').val(parseInt(jQuery('.nbr_etape').val()) + 1);
            jQuery('.etape-menu').append(data);
            jQuery('.link-etape:not(.bound)').addClass('bound')
                .on('click',  evenement_activer);
            jQuery('.supprimerEtape:not(.bound)').addClass('bound')
                .on('click',  supprimer_etape);

            var etape_id = (jQuery('.etape-menu li:last-child')
                .data('etapeid'));
            jQuery('<div class="contenu-etape" data-etapeid="'
                +etape_id+'"></div>').insertAfter('.etape-menu');
            jQuery('.etape-menu li:last-child .link-etape').click();
        },
        error:function(jqXHR, textStatus, errorThrown){
            //console.log(jqXHR);
        }
    });
}

/**
 * Ajoute le dennier id de la section dans la div
 */
function ajouter_section_id() {
    var section_id = jQuery('.section_id').val();
    section_id = parseInt(section_id) + 1;
    jQuery('.section_id').val(section_id);
}

/**
 * Copie le lien de partage
 */
function lien_de_partage() {
    var lien = jQuery(this).val();
    document.execCommand("copy");
}

/**
 * Initialise tout les évènements du programme
 */
function initialiser_evenement() {
    jQuery('.condition:not(.bound)').addClass('bound')
        .on('click',  condition_reponse);
    jQuery('.supprimerEtape:not(.bound)').addClass('bound')
        .on('click',  supprimer_etape);
    jQuery('.link-etape:not(.bound)').addClass('bound')
        .on('click',  evenement_activer);
    jQuery('#ajouter_champs:not(.bound)').addClass('bound')
        .on('click',  ajouter_champs);
    jQuery('.afficher_question option:not(.bound)').addClass('bound')
        .on('click',  ajout_reponse_condition);
    jQuery('.supprimer_question:not(.bound)').addClass('bound')
        .on("click", supprimer_question);
    jQuery('.bouton-question:not(.bound)').addClass('bound')
        .on('click', ajouter_question);
    jQuery('.noteAffilie:not(.bound)').addClass('bound')
        .on("click", modifier_note);
    jQuery('.DeleteNote:not(.bound)').addClass('bound')
        .on("click", supprimer_note);
    jQuery('#buttonAjouterNote:not(.bound)').addClass('bound')
        .on("click", ajouter_note);
    jQuery('.btnEtape:not(.bound)').addClass('bound')
        .on("click", etape_suivante);
    jQuery('.btnEtapePrecedent:not(.bound)').addClass('bound')
        .on("click", etape_precedente);
    jQuery('.ajouter-etape:not(.bound)').addClass('bound')
        .on("click", ajouter_etape);
    jQuery('#checkbox:not(.bound), #bouton_radio:not(.bound)')
        .addClass('bound')
        .on('click', ajouter_section_id);
    jQuery('#lienPartageAccueil:not(.bound)').addClass('bound')
        .on('click', lien_de_partage);
    jQuery("#buttonsubmitForm").on("click", function(){
        jQuery("#formClient").submit(); });
    jQuery('.questionFormulaireClient').change(function(){
        form_afficher_question(); });

}



/**
 * Initailise les data table pour l'affichage
 */
function initialise_data_table() {
    jQuery('.liste_table_paiement').DataTable({
        dom: 'lfrBtip',
        buttons: {
            buttons: ['pdf']
        },
        "language": {
            "url": "/js/french.json",
        },
    });
    jQuery('.liste_table').DataTable({
        "language": {
            "url": "/js/french.json",
        },
    });
}


/**
 * Veifie si toute les questions sont remplis
 *
 * @param e
 */
function verif_nom_question(e) {
    jQuery('.nom_question').each(function() {
        if (!jQuery(this).val()) {
            e.preventDefault();
            alert('Veuillez remplir tout les champs');
            return false;
        }
    });
}

/**
 * Veifie si toute les sections sont remplis
 *
 * @param e
 */
function verif_section(e) {
    jQuery('.selection').each(function() {
        if (!jQuery(this).val()) {
            e.preventDefault();
            alert('Veuillez remplir tout les champs');
            return false;
        }
    });
    jQuery('.montant-ajouter').each(function() {
        if (!jQuery(this).val()) {
            e.preventDefault();
            alert('Veuillez remplir tout les champs');
            return false;
        }
    });
}

$.noConflict();

/**
 * Initialise les composant une fois que la fois est chargée
 */
jQuery(document).ready(function(){

    initialiser_evenement();
    form_afficher_question();
    form_afficher_etape(1);
    initialise_data_table();

    jQuery.ajaxSetup({
        headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]')
                .attr('content') }
    });

    jQuery('.etape-menu li:first-child').addClass('active');
    jQuery('.contenu-etape:first').addClass('active');

    jQuery('#formulaire').on('submit', function(e) {
        verif_nom_question(e);
        verif_section(e);
    });

});
