
<?php $i = $nbr_question;
?>
<div class="wrapper-question" id="input" data-questionid="{{$dernier_id}}">
    <input name="nombre_question" id="nombre_question" type="hidden" value="{{$i}}">
    <input type="hidden" name="etape_id{{$i}}" value="{{$etape_id}}">
    <input type="hidden" name="question-id{{$i}}" value="new">
    <input type="hidden" name="type{{$i}}" value="{{$type_question}}">
    <span class="type_question">
        @if ($type_question == 'bouton_radio')
            Bouton radio
        @elseif($type_question == 'input')
            Input
        @elseif($type_question == 'zone_texte')
            Zone de texte
        @elseif($type_question == 'checkbox')
            Checkbox
        @endif
    </span>
    <label for="nom_question{{$i}}">Nom</label>
    @if(($type_question == 'checkbox') || ($type_question == 'bouton_radio'))
        <input type="text" name="nom_question{{$i}}" class="nom_question multiple">
        <table>
            <tr>
                <th>Nom du checkbox</th>
                <th>Montant à ajouter</th>
            </tr>
            <tr>
                <td><input class="reponse-multiple{{$dernier_id}} selection" name="reponse_multiple1{{$dernier_id}}" type="text"></td>
                <td><input type="number" class="montant_ajouter" name="montant_ajouter1{{$dernier_id}}"></td>
                <td><a href="javascript:void(0)" class="reponse1{{$dernier_id}} supp_reponse new" onclick="supprimer_reponse(1{{$dernier_id}})">-</a></td>
                <input type="hidden" class="section-id" name="section-id1{{$dernier_id}}" value="{{$section_id+1}}">
            </tr>
        </table>
        <input type="hidden" value="1" name="nbr_reponse{{$dernier_id}}" class="nbr_reponse reponse_info">
        <a href="javascript:void(0)" id="ajouter_champs" class="ajouter_information">Ajouter</a>
    @else
        <input type="text" name="nom_question{{$i}}" class="nom_question">
    @endif
    <div class="affichersi" data-etapeid="{{$etape_id}}">
        <label>Logique conditionnelle</label>
        <input type="checkbox" name="condition-verification{{$i}}" id="condition" class="condition"><br>
        <div class="wrapper-condition">
            <label>Afficher si</label>
             <select class="afficher_question" name="afficher_question{{$i}}">
                 <option value=""></option>
                @foreach($questions as $question_affiche)
                    @if ($question_affiche->etape_id == $etape_id)
                         @if (($question_affiche->type === "checkbox") || (($question_affiche->type === "bouton_radio")))
                             <option value="{{$question_affiche->id}}">{{$question_affiche->nom}}</option>
                         @endif
                    @endif
                @endforeach
            </select>
            <span>EST</span>
            <select class="afficher_reponse" name="afficher_reponse{{$i}}">
                <option value=""></option>
            </select>
        </div>
    </div>
    <a href="javascript:void(0)" data-questionnbr="{{$i}}" data-questionid="new" class="supprimer_question">Supprimer la question</a>
</div>







