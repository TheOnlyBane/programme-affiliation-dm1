@extends('layouts.app')
@section('content')
<h1>Modification de note</h1>
<form method="POST" action="/note/{{$note->id}}">
    @method('PATCH')
    @csrf
    @if($errors->any())
        <div class="notification is-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="control">
            <textarea name="description">{{$note->description}}</textarea>
    </div>
    <button type="submit">Mise à jour</button>
@endsection