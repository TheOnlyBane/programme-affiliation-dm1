<tbody id="note{{$uneNote->id}}">
    <tr>
        <td>{{$uneNote->date}}</td>
        <td><textarea class="form-control note" {{Auth::user()->isAdmin !=1 ? 'readonly':'true'}} data-noteID="{{$uneNote->id}}" data-affilie_id="{{$uneNote->affilie_id}}" class='inputUpdateNote{{Auth::user()->isAdmin != 1 ? '1' : ''}}' >{{$uneNote->description}}</textarea></td>
        <td><a data-noteID="{{$uneNote->id}}" class="DeleteNote">Supprimer</a></td>
    </tr>
</tbody>
