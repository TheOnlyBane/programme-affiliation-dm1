@extends('layouts.app')
@section('content')
    <h1>Liste projets</h1>
    <table class="liste_table">
        <thead>
        <tr>
                    <th>Projet id</th>
                    <th>Type de projet</th>
                    <th>Status</th>
                    <th>Affilié</th>
            </tr>
        </thead>
        <tbody>
            @foreach($projets as $projet)
                <tr>
                    <?php

                    $user = $projet->affilie ;
                    $user = $user->user;

                    ?>
                    <td><a href="/projet/{{$projet->id}}/edit">{{$projet->id}}</a></td>
                    <td>{{$projet->type}}</td>
                    <td>{{$projet->status}}</td>
                    <td>{{$user->name}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <button class="buttonForm btn btn-primary" onclick="window.location='{{url("projet/create")}}'">Ajouter un projet</button>

@stop
