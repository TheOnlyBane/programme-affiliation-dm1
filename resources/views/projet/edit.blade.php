@extends('layouts.app')
@section('content')
<h1>Modifier le projet</h1>
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<form method="POST" action="/projet/{{ $projet->id }}" style="margin-bottom: 10px;">
    @method('PATCH')
    @csrf

    <div class="form-group">
        <label class="label" for="type">Type</label>
        <input type="text" class="form-control" name="type" placeholder="Type" value="{{ $projet->type }}">
    </div>
    <div class="form-group">
        <label class="label" for="nom">Nom</label>
        <input type="text" class="form-control" name="nom" placeholder="Nom" value="{{ $projet->nom }}">
    </div>

    <div class="form-group">
        <label class="label" for="title">Status</label>
        <select name="status" class="form-control">
            <option value="en_attente" {{$projet->status == 'en_attente' ? 'selected' : ''}}>En attente</option>
            <option value="en_cours" {{$projet->status == 'en_cours' ? 'selected' : ''}}>En cours</option>
            <option value="termine" {{$projet->status == 'termine' ? 'selected' : ''}}>Terminé</option>
        </select>
    </div>

    <div class="form-group">
        <label class="label" for="date">Date</label>
        <input type="date" class="form-control" name="date" value={{ $projet->date }}>
    </div>

    <div class="form-group">
        <label class="label" for="commission">Commission</label>
        <input type="text" class="form-control" name="commission" placeholder="commissionPourcentage"
            value="{{ $projet->commissionPourcentage }}">
    </div>
    <div class="form-group">
        <label class="label" for="montantTotal">Montant Total</label>
        <input type="text" class="form-control" name="montantTotal" placeholder="Montant Total"
            value="{{ $projet->montantTotal }}">
    </div>
    <div class="form-group">
        <label class="label" for="montantActuel">Montant Actuel</label>
        <input type="text" readonly class="form-control" name="montantActuel" placeholder="Montant Actuel"
            value="{{ $projet->montantActuel }}">
    </div>

    <div class="form-group">
        <label class="label" for="affilie">Affilié</label>
        <select name="affilie" class="form-control">
            @foreach ($affilies as $affilie)
                @if ($affilie->id == $projet->affilie_id)
                    <option class="form-control" value="{{$affilie->id}}" selected>{{$affilie->user->name}}</option>
                @else
                    <option class="form-control" value="{{$affilie->id}}">{{$affilie->user->name}}</option>
                @endif
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <button type="submit" class="button btn btn-primary">Mettre à jour</button>
    </div>
</form>


<form method="POST" action="/projet/{{ $projet->id }}">
    @method('DELETE')
    @csrf

    <div class="form-group">
        <button type="submit" class="button btn btn-danger">Supprimer le projet</button>
    </div>
</form>

<a href="../">Retour</a>
@stop
