@extends('layouts.app')
@section('content')
<h1>Ajouter un projet</h1>

@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<form method="POST" action="/projet" style="margin-bottom: 10px;">
    @method('POST')
    @csrf

    <div class="form-group">
        <label class="label" for="type">Type</label>

        <input type="text" class="form-control" name="type" value="{{old('type')}}">
    </div>
    <div class="form-group">
        <label class="label" for="nom">Nom</label>

        <input type="text" class="form-control" name="nom" value="{{old('nom')}}">
    </div>

    <div class="form-group">
        <label class="label" for="title">Status</label>
        <select name="status" class="form-control">
            <option value="en_attente">En attente</option>
            <option value="en_cours">En cours</option>
            <option value="termine">Terminé</option>
        </select>
    </div>

    <div class="form-group">
        <label class="label" for="date">Date</label>
        <input type="date" class="form-control" name="date" value="<?php echo date('Y-m-d'); ?>">
    </div>

    <div class="form-group">
        <label class="label" for="commission">Commission</label>

        <input type="text" class="form-control" name="commission" value="{{old('commission')}}">
    </div>
    <div class="form-group">
        <label class="label" for="montantTotal">Montant Total</label>

        <input type="text" class="form-control" name="montantTotal" value="{{old('montantTotal')}}">
    </div>

    <div class="form-group">
        <label class="label" for="affilie">Affilié</label>

        <select class="form-control" name="affilie">
            @foreach ($affilies as $affilie)
                <option class="form-control" value="{{$affilie->id}}">{{$affilie->user->name}}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-primary">Ajouter le projet</button>
    </div>
</form>

@stop
