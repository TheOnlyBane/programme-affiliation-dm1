@extends('layouts.app')
@section('content')
<h1>Ajouter un affilié</h1>
<form method="POST" action="/affilie/">
        {{csrf_field()}}
        <h2>Informations d'utilisateur</h2>
        @if($errors->any())
        <div class="notification is-danger">
                <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{$error}}</li>
                        @endforeach
                </ul>
        </div>
        @endif
        <div  class="form-group">
                <label class="label" for="type">Nom</label>
                <input class="form-control" type="text" {{$errors->has('name') ? 'is-danger' : ''}} name="name" value="{{old('name')}}"  />
        </div>
        <div  class="form-group">
                <label class="label" for="type">Email</label>
                <input class="form-control" type="text" {{$errors->has('email') ? 'is-danger' : ''}} name="email" value="{{old('email')}}" />
        </div>
        <div  class="form-group">
                <label class="label" for="type">Mot de passe</label>
                <input class="form-control" type="password" {{$errors->has('password') ? 'is-danger' : ''}}
                        name="password" />
        </div>
        <div class="form-group">
                <label class="label" for="type">Confirmation du mot de passe</label>
                <input class="form-control" type="password" {{$errors->has('password_confirmation') ? 'is-danger' : ''}} name="password_confirmation"/>
            </div>
        <div  class="form-group"><label class="label" for="type">telephone</label>
                <input class="form-control" type="telephone" {{$errors->has('telephone') ? 'is-danger' : ''}}
                        name="telephone" value="{{old('telephone')}}" />
        </div>

        <div  class="form-group">
                <label class="label" for="type">Adresse</label>
                <textarea class="form-control" name="adresse">{{old('adresse')}}</textarea>
        </div>

        <div  class="form-group"><label class="label" for="type">Appartement</label>
                <input class="form-control" type="app" {{$errors->has('app') ? 'is-danger' : ''}} name="app" value="{{old('app')}}" />
        </div>

        <div  class="form-group"><label class="label" for="type">Code Postal</label>
                <input class="form-control" type="codePostal" {{$errors->has('codePostal') ? 'is-danger' : ''}}
                        name="codePostal" value="{{old('codePostal')}}" />
        </div>

        <div  class="form-group"><label class="label" for="type">Provenance</label>
                <select name="ProvenanceID" class="form-control">
                        @foreach ($Provenances as $provenance)
                        <option value={{$provenance->id}}>{{$provenance->pays}} , {{$provenance->province}}
                        </option>
                        @endforeach
                </select>
        </div>
        <h2>Informations d'Entreprise</h2>
        <div  class="form-group"><label class="label" for="type">Numéro de TPS</label>
                <input class="form-control" type="numTPS" {{$errors->has('numTPS') ? 'is-danger' : ''}} name="numTPS" value="{{old('numTPS')}}"/>
        </div>

        <div  class="form-group">
                <label class="label" for="type">Numéro de TVQ</label>
                <input class="form-control" type="numTVQ" {{$errors->has('numTVQ') ? 'is-danger' : ''}} name="numTVQ" value="{{old('numTVQ')}}" />
        </div>

        <div  class="form-group">
                <label class="label" for="type">Nom de l'Entreprise</label>
                <input class="form-control" type="nomEntreprise" {{$errors->has('nomEntreprise') ? 'is-danger' : ''}}
                        name="nomEntreprise" value="{{old('nomEntreprise')}}" />
        </div>

        <div  class="form-group">
                <label class="label" for="type">Numéro d'Entreprise</label>
                <input class="form-control" type="numEntreprise" {{$errors->has('numEntreprise') ? 'is-danger' : ''}}
                        name="numEntreprise" value="{{old('numEntreprise')}}" />
        </div>

        <div  class="form-group">
                <label class="label" for="type">Numéro de téléphone de l'Entreprise</label>
                <input class="form-control" type="numTelEntreprise"
                        {{$errors->has('numTelEntreprise') ? 'is-danger' : ''}} name="numTelEntreprise" value="{{old('numTelEntreprise')}}" />
        </div>

        <div  class="form-group">
                <label class="label" for="type">Commission Récurrente</label>
                <input class="form-control" type="commReccurent" {{$errors->has('commissionReccurent') ? 'is-danger' : ''}}
                        name="commissionReccurent"  value="{{old('commissionReccurent')}}"/>
        </div  class="form-group">

        <div  class="form-group">
                <label class="label" for="type">Commission Simple</label>
                <input class="form-control" type="commissionSimple" {{$errors->has('commissionSimple') ? 'is-danger' : ''}}
                        name="commissionSimple" value="{{old('commissionSimple')}}"/>
        </div>

        <div  class="form-group">
                <label class="label" for="type">Solde</label>
                <input class="form-control" type="solde" {{$errors->has('solde') ? 'is-danger' : ''}} name="solde" value="{{old('solde')}}" />
        </div>

        <button class="btn btn-primary" type="submit">Envoyer</button>

</form>
@stop