@extends('layouts.app')
@section('content')
    <h1>Liste des affiliés</h1>
    <table class="liste_table">
       <thead>
            <tr>
                <th>Nom</th>
                <th>Téléphone</th>
                <th>Courriel</th>
                <th>Solde</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($affilies as $affilie)
            <tr>
                <td><a href="/affilie/{{$affilie->id}}/edit">{{$affilie->user->name}}</a></td>
                <td>{{$affilie->user->telephone}}</td>
                <td>{{$affilie->user->email}}</td>
                <td>{{$affilie->solde}}$</td>
            </tr>
            @endforeach
        </tbody>
    </table>

    <button class="buttonForm btn btn-primary" onclick="window.location='{{url("affilie/create")}}'">Ajouter un Affilie</button>
@stop
