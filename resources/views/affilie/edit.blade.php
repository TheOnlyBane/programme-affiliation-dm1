@extends('layouts.app')
@section('content')
<h1>Profil de {{$user->name}}</h1>
<div>
    @if ($isAdmin == 1)
    <form method="POST" action="/affilie/{{$affilie->id}}">
    @else
    <form method="POST" action="/affilie/updateUser/{{$affilie->id}}">
    @endif
        {{method_field('PATCH')}}
        {{csrf_field()}}
        @if($errors->any())
            <div class="notification is-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <h2>Informations de l'utilisateur</h2>
        <div class="form-group">
            <label class="label" for="type">Nom</label>
            <input class="form-control" type="text" {{$errors->has('name') ? 'is-danger' : ''}} name="name"value="{{$user->name}}"/>
        </div>
        <div class="form-group">
                <label class="label" for="type">Courriel</label>
                <input class="form-control" readonly type="text" {{$errors->has('email') ? 'is-danger' : ''}} name="email"value="{{$user->email}}"/>
        </div>
        <div class="form-group">
                <label class="label" for="type">Mot de passe</label>
        <input class="form-control" type="password" {{$errors->has('password') ? 'is-danger' : ''}} placeholder="Mot de passe" name="password"/>
        </div>
        <div class="form-group">
            <label class="label" for="type">Confirmation du mot de passe</label>
        <input class="form-control" type="password" {{$errors->has('password_confirmation') ? 'is-danger' : ''}} placeholder="Confirmation du mot de passe" name="password_confirmation"/>
        </div>
        <div class="form-group">
                <label class="label" for="type">Téléphone</label>
                <input class="form-control" type="telephone" {{$errors->has('telephone') ? 'is-danger' : ''}} name="telephone" value="{{$user->telephone}}"/>
        </div>
        <div class="form-group">
                <label class="label" for="type">Adresse</label>
                <textarea class="form-control" name="adresse"placeholder="Adresse">{{$user->adresse}}</textarea>
        </div>
        <div class="form-group">
                <label class="label" for="type">Appartement</label>
                <input class="form-control" type="app" {{$errors->has('app') ? 'is-danger' : ''}} name="app" value="{{$user->app}}"/>
        </div>
        <div class="form-group">
                <label class="label" for="type">Code postal</label>
                <input class="form-control" type="codePostal" {{$errors->has('codePostal') ? 'is-danger' : ''}} name="codePostal" value="{{$user->codePostal}}"/>
        </div>
        <div class="form-group">
            <label class="label" for="type">Provenance</label>
            <select class="form-control" name="ProvenanceID">
                @foreach ($provenances as $provenance)
                @if ($provenance->id == $user->provenance_id)
                <option selected value={{$provenance->id}}>{{$provenance->pays}} , {{$provenance->province}}</option>
                @else
                <option value={{$provenance->id}}>{{$provenance->pays}} , {{$provenance->province}}</option>
                @endif
                @endforeach
            </select>
        </div>
        <h2>Informations de l'entreprise</h2>
        <div class="form-group">
                <label class="label" for="type">Numéro de TPS</label>
                <input class="form-control" type="numTPS" {{$errors->has('numTPS') ? 'is-danger' : ''}} name="numTPS" value="{{$affilie->numTPS}}"/>
        </div>
        <div class="form-group">
                <label class="label" for="type">Numéro de TVQ</label>
                <input class="form-control" type="numTVQ" {{$errors->has('numTVQ') ? 'is-danger' : ''}} name="numTVQ" value="{{$affilie->numTVQ}}"/>
        </div>
        <div class="form-group">
                <label class="label" for="type">Nom d'entreprise</label>
                <input class="form-control" type="nomEntreprise" {{$errors->has('nomEntreprise') ? 'is-danger' : ''}} name="nomEntreprise" value="{{$affilie->nomEntreprise}}"/>
        </div>
        <div class="form-group">
                <label class="label" for="type">Nom de l'entreprise</label>
                <input class="form-control" type="numEntreprise" {{$errors->has('numEntreprise') ? 'is-danger' : ''}} name="numEntreprise" value="{{$affilie->numEntreprise}}"/>
        </div>
        <div class="form-group">
                <label class="label" for="type">Numéro de téléphone de l'entreprise</label>
                <input class="form-control" type="numTelEntreprise" {{$errors->has('numTelEntreprise') ? 'is-danger' : ''}} name="numTelEntreprise" value="{{$affilie->numTelEntreprise}}"/>
        </div>
        <div class="form-group">
                <label class="label" for="type">Commission récurrente</label>
                <input class="form-control" type="commissionReccurent" {{$errors->has('commissionReccurent') ? 'is-danger' : ''}} name="commissionReccurent" value="{{$affilie->commissionReccurent}}"/>
        </div>
        <div class="form-group">
                <label class="label" for="type">Commission simple</label>
                <input class="form-control" type="commissionSimple" {{$errors->has('commissionSimple') ? 'is-danger' : ''}} name="commissionSimple" value="{{$affilie->commissionSimple}}"/>
        </div>
        <div class="form-group">
                <label class="label" for="type">Solde</label>
                <input class="form-control" type="solde" {{$errors->has('solde') ? 'is-danger' : ''}} name="solde" value="{{$affilie->solde}}"/>
        </div>
        <button class="btn btn-primary" type="submit">Mise à jour</button>
    </form>
    <h2>Notes</h2>
    <div>
        <table id="TableNote" class="liste_table">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Note</th>
                    <th>Supprimer</th>
                </tr>
            </thead>
        @foreach ($notes as $uneNote)
        @if ($uneNote->affilie_id == $affilie->id)
            <tbody id="note{{$uneNote->id}}" class="">
                <tr>
                    <td>{{$uneNote->date}}</td>
                    <td><textarea class="form-control noteAffilie" {{Auth::user()->isAdmin !=1 ? 'readonly':'true'}} data-noteID="{{$uneNote->id}}" data-affilie_id="{{$uneNote->affilie_id}}" class='inputUpdateNote{{Auth::user()->isAdmin != 1 ? '1' : ''}}' >{{$uneNote->description}}</textarea></td>
                    <td><a data-noteID="{{$uneNote->id}}" class="DeleteNote">Supprimer</a></td>
                </tr>
            </tbody>
        @endif
        @endforeach
        </table>
    </div>
    @if($isAdmin == 1)
        <h3>Ajouter une note</h3>
        <div class="form-group">
            <textarea class="form-control noteDescription" id="descriptionNouvelleNote" name="description"placeholder="Description">{{old('description')}}</textarea>
        </div>
        <button id="buttonAjouterNote" class="btn btn-primary" data-affilie_id="{{$affilie->id}}" type="submit" class="button">Ajouter une note</button>
        <form method="POST" action="/affilie/{{$affilie->id}}">
            @method('DELETE')
            @csrf
                <div class="form-group">
                    <button type="submit" class="button btn btn-danger">Supprimer un affilié</button>
                </div>
        </form>
    @endif


</div>
@stop
