@extends('layouts.app')
@section('content')
    <h1>Liste de paiements</h1>
    <table id="#paiementTable" class="liste_table_paiement">
        <thead>
            <th>ID</th>
            <th>Nom du projet</th>
            <th>Nom de l'affilié</th>
            <th>Date de facturation</th>
            <th>Type de projet</th>
            <th>Type de paiement</th>
            <th>Montant à payer</th>
            @if (Auth::user()->isAdmin == 1)
            <th>Lien du projet</th>
            @endif
            <th>Lien de facture</th>
        </thead>
        <tbody>
        @if (Auth::user()->isAdmin == 1)
            @foreach ($paiements as $paiement)
                <tr>
                    <td><a href='/paiement/{{$paiement->id}}/edit'>{{$paiement->id}}</a></td>
                    <td>{{$paiement->projet->nom}}</td>
                    <td>{{$paiement->projet->affilie->user->name}}</td>
                    <td>{{$paiement->date}}</td>
                    <td>{{$paiement->projet->type}}</td>
                    <td>{{$paiement->type}}</td>
                    <td>{{$paiement->montant}}</td>
                    <td><a href='/projet/{{$paiement->projet->id}}/edit'>Projet</a></td>
                    <td><a href="/facture/{{$paiement->id}}">Facture</a></td>
                </tr>
            @endforeach
        @else
            @foreach ($paiements as $paiement)
                <tr>
                    <td>{{$paiement->id}}</td>
                    <td>{{$paiement->projet->nom}}</td>
                    <td>{{$paiement->projet->affilie->user->name}}</td>
                    <td>{{$paiement->date}}</td>
                    <td>{{$paiement->projet->type}}</td>
                    <td>{{$paiement->type}}</td>
                    <td>{{$paiement->montant}}</td>
                    <td><a href="/facture/{{$paiement->id}}">Facture</a></td>
                </tr>
            @endforeach
        @endif
            
        </tbody>
    </table>
@if (Auth::user()->isAdmin == 1)
<button class="buttonForm btn btn-primary" onclick="window.location='{{url("paiement/create")}}'">Ajouter un paiement</button>
@endif
@stop
