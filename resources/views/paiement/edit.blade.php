@extends('layouts.app')
@section('content')
    <h1>Modifier un paiement</h1>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form method="POST" action="/paiement/{{$paiement->id}}" style="margin-bottom: 10px;">
        @method('PATCH')
        @csrf

        <div class="form-group">
            <label class="label" for="type">Type</label>

                <input type="text" class="form-control" name="type" value="{{$paiement->type}}" >
        </div>

        <div class="form-group">
            <label class="label" for="montant">Montant</label>

                <input type="number" class="form-control" name="montant" value="{{$paiement->montant}}">
        </div>

        <div class="form-group">
            <label class="label" for="date">Date</label>
                <input type="date" class="form-control" name="date" value="{{$paiement->date}}">
        </div>

        <div class="form-group">
            <label class="label" for="projet">Projet</label>

                <select name="projet" class="form-control">
                    @foreach ($projets as $projet)
                        @if ($projet->id == $paiement->projet_id)
                            <option class="form-control" selected value="{{$projet->id}}">{{$projet->id}}</option>
                        @else
                            <option class="form-control" value="{{$projet->id}}">{{$projet->id}}</option>
                        @endif
                    @endforeach
                </select>
        </div>

        <div class="form-group">
                <button type="submit" class="button btn btn-primary">Modifier</button>
        </div>
    </form>

    <form method="POST" action="/paiement/{{ $paiement->id }}">
        @method('DELETE')
        @csrf

        <div class="form-group">
                <button type="submit" class="button btn btn-danger">Supprimer</button>
        </div>
    </form>

@stop
