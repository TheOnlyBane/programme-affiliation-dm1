@extends('layouts.app')
@section('content')
<h1>Ajouter un Paiement</h1>

@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<form method="POST" action="/paiement" style="margin-bottom: 10px;">
    @method('POST')
    @csrf

    <div class="form-group">
        <label class="label" for="type">Type</label>

        <input type="text" class="form-control" name="type" value="{{old('type')}}">
    </div>

    <div class="form-group">
        <label class="label" for="montant">Montant</label>

        <input type="number" class="form-control" name="montant" value="{{old('montant')}}">
    </div>

    <div class="form-group">
        <label class="label" for="date">Date</label>
        <input type="date" class="form-control" name="date" value="<?php echo date('Y-m-d'); ?>">
    </div>

    <div class="form-group">
        <label class="label" for="projet">Projet</label>

        <select name="projet" class="form-control">
            @foreach ($projets as $projet)
                <option value="{{$projet->id}}">{{$projet->id}}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-primary">Ajouter le projet</button>
    </div>
</form>

@stop