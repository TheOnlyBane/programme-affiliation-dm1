@extends('layouts.app')

@section('content')
@if (Auth::check())
    <div class="container">
        <div class="row justify-content-center">
            <div class="accueil">
                @if (Auth::user()->isAdmin != 1)

                <!-- Partie utilisateur-->
                    <div class="card dashboard">
                        <div class="card-header">Tableau de bord</div>
                            <div class="Stats">
                                <div class=" card cardStatistique ">
                                    <h3 class="card-header card-stat">Nombre de Clic</h3>
                                    <p>{{$user->affilie->statistique->nombreClic}}</p>
                                </div>
                                <div class="card cardStatistique">
                                    <h3 class="card-header card-stat">Nombre de Soumission</h3>
                                    <p>{{$user->affilie->statistique->nombreSoumission}}</p>
                                </div>
                                <div class="card cardStatistique">
                                    <h3 class="card-header card-stat">Nombre de Commande</h3>
                                    <p>{{$user->affilie->statistique->nombreCommande}}</p>
                                </div>
                                <div class="Accueil">
                                    <h3 class="labelAccueil">Lien de partage:</h3>
                                    <input id="lienPartageAccueil" class="form-control inputAccueil" readonly value="https://elefen.com/soumission/{{Auth::user()->affilie->lienPartage}}">
                                </div>
                                <div class="Accueil">
                                    <h3 class="labelAccueil">Solde:</h3>
                                    <input class="form-control inputAccueil" readonly value="{{$user->affilie->solde}}">
                                </div>
                                <div class="Accueil">
                                    <h3 class="labelAccueil">Commission Reccurente:</h3>
                                    <input class="form-control inputAccueil" readonly value="{{$user->affilie->commReccurent}}%">
                                </div>
                                <div class="Accueil">
                                    <h3 class="labelAccueil">Commission Simple:</h3>
                                    <input class="form-control inputAccueil" readonly value="{{$user->affilie->commSimple}}%">
                                </div>
                                @if ($user->affilie->note->count() > 0)
                                    <div class="Accueil">
                                        <h3 class="labelAccueil">Dernière Note:</h3>
                                        <textarea class="form-control inputAccueil noteAccueil" readonly >{{DB::table('notes')
                                                ->where('affilie_id', '=', $user->affilie_id)
                                                ->latest('date')
                                                ->get()[0]->description
                                        }}</textarea>
                                    </div>
                                @endif
                            </div>
                        </div>
                @else
                <!-- Partie Administrateur -->
                @include('paiement.show', ['projets' => $projets, 'paiements' => $paiements])
                @endif
                </div>
            </div>
        </div>
    </div>
@endif
@endsection
