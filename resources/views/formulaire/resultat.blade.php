@extends('layouts.form')

@section('content')
    <div class="progress">
            <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 100%; background-color: #f07917"><span>100%</span></div>
    </div>
    <div class="resultatFormulaire">
        <h2>Merci d'avoir fait affaire avec nous pour la création de votre projet web.</h2>
        <h3>Le montant approximatif de votre projet est : {{$projet->montantTotal}}$</h3>
        <a class="btn btn-primary" href ="https://www.elefen.com">Retour à l'accueil</a>
    </div>
    
@endsection