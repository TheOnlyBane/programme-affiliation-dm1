@extends('layouts.form')

@section('content')

<form method="POST" id="formClient" name="formClient" action="/reponse/" >
    @method('POST')
    @csrf
        <input type="hidden" name="affilie" value="{{$affilie->id}}">
        <input type="hidden" name="formulaire" value="{{$formulaire->id}}">
        <!-- Pour les réponses -->
        <div id="nombreEtapeFormulaire" data-nombreEtape= {{count($formulaire->etape)}}></div>
        <div class="progress">
            <div id="progressBar" class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                <span id="progressStatus"></span>
            </div>
        </div>
        
        @foreach ($formulaire->etape as $etape)
        @if ($etape->question != null)
            <div id="etape{{$affichageEtape[$etape->id]}}">
                <h2>Etape {{$affichageEtape[$etape->id]}}</h2>
                @foreach ($etape->question as $question)
                    @if ($question->type == 'checkbox')
                        <div id="question{{$question->id}}" data-parent="{{$question->conditionParent}}" data-section="{{$question->conditionSection}}" class="form-group form-check-client">
                            <label class="label form-label-client ">{{$question->nom}}</label>
                            @foreach ($question->section as $section)
                                <div class="form-check">
                                    <input id="question{{$question->id}}-{{$section->id}}" class="form-check-input questionFormulaireClient" data-montant={{$section->montant}} name="question{{$question->id}}-{{$section->id}}" type="checkbox" value="{{$section->information}}">
                                    <label class="form-check-label" for="question{{$question->id}}-{{$section->id}}">{{$section->information}}</label>
                                </div>
                            @endforeach
                        </div>
                    @elseif($question->type == 'bouton_radio')
                        <div id="question{{$question->id}}" data-parent="{{$question->conditionParent}}" data-section="{{$question->conditionSection}}" class="form-group form-check-client">
                            <label class="label form-label-client ">{{$question->nom}}</label><br>
                            <div class="form-check">
                                @foreach ($question->section as $section)
                                    <input id="question{{$question->id}}-{{$section->id}}" class="form-check-input questionFormulaireClient" data-montant={{$section->montant}} name="question{{$question->id}}" type="radio" value="{{$section->information}}">
                                    <label class="form-check-label" for="question{{$question->id}}">{{$section->information}}</label>
                                @endforeach
                            </div>
                        </div>
                    @elseif($question->type == 'input')
                        <div id="question{{$question->id}}" data-parent="{{$question->conditionParent}}" data-section="{{$question->conditionSection}}" class="form-group form-check-client">
                            <label class="label form-label-client ">{{$question->nom}}</label><br>
                            <input class="form-control form-input-client" name="question{{$question->id}}"><br>
                        </div>
                    @elseif($question->type == 'zone_texte')
                        <div id="question{{$question->id}}" data-parent="{{$question->conditionParent}}" data-section="{{$question->conditionSection}}" class="form-group form-check-client">
                            <label class="label form-label-client ">{{$question->nom}}</label><br>
                            <textarea class="form-control form-textarea-client" name="question{{$question->id}}"></textarea><br> 
                        </div>
                        
                    @endif
                @endforeach
                <a id="buttonEtape{{$affichageEtape[$etape->id]}}"  data-id={{$affichageEtape[$etape->id]}} class="btn btn-primary btnEtapePrecedent">Etape précédente</a>
                @if ($affichageEtape[$etape->id] == count($formulaire->etape))
                <a id="buttonsubmitForm" data-id={{$affichageEtape[$etape->id]}} class="btn btn-primary btnEtape">Envoyer !</a>
                @else
                <a id="buttonEtape{{$affichageEtape[$etape->id]}}"  data-id={{$affichageEtape[$etape->id]}} class="btn btn-primary btnEtape">Etape suivante</a>
                @endif
            </div>
        @endif
        @endforeach
        <div id="nombreQuestionFormulaire" data-nombreQuestion = {{$maxQuestion}}></div>
    </form>
    </div>
@endsection