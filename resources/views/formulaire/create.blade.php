@extends('layouts.app')
@section('content')

    <h1>Ajouter un Formulaire</h1>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form method="POST" action="/formulaire" style="margin-bottom: 10px;">
        @method('POST')
        @csrf

        <div class="field">
            <label class="label" for="nom" >Nom</label>

            <div class="control">
                <input type="text" class="input" name="nom" value="{{old('nom')}}">
            </div>
        </div>

        <div class="field">
            <div class="control">
                <button type="submit" class="button is-link">Ajouter le formulaire</button>
            </div>
        </div>
    </form>

@stop

