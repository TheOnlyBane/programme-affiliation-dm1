@extends('layouts.app')
@section('content')

    <h1>Ajouter un Formulaire</h1>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form method="POST" name="form1" id="formulaire" class="formulaire" action="/formulaire/{{$formulaire->id}}" style="margin-bottom: 10px;">
        @method('PATCH')
        @csrf

        <div class="field">
            <label class="label" for="nom">Nom</label>
            <div class="control">
                <input type="text" class="input" name="nom" value="{{$formulaire->nom}}">
            </div>
        </div>

        <div class ="wrapper-etape">
            <ul class="etape-menu" data-formulaireid="{{$formulaire->id}}">
                <?php $i = 1;
                $etapes = $formulaire->etape;
                $z = 0;
                ?>
                @foreach($etapes as $etape)
                        <li data-etapenbr="{{$i}}" data-etapeid="{{$etape->id}}">
                            <a class="link-etape">Étape {{$i}}</a>
                            <a href="JavaScript:void(0)" class="supprimerEtape">X</a>
                        </li>
                       <?php $i++; ?>
                @endforeach
            </ul>
            <input type="hidden" name="nbr_etape" class="nbr_etape" value="{{$i}}">
            @foreach($etapes as $etape)
                <?php $questions = $etape->question; ?>
                <div class="contenu-etape" data-etapeid="{{$etape->id}}">
                    @foreach ($questions as $question)
                        <?php
                        $y = 0;
                        $z++;
                        ?>
                        <div class="wrapper-question" id="input"  data-questionid="{{$question->id}}">
                            <input type="hidden" name="question-id{{$z}}" value="{{$question->id}}">
                            <input type="hidden" name="etape_id{{$z}}" value="{{$question->etape_id}}">
                            <input type="hidden" name="type{{$z}}" value="{{$question->type}}">
                            <input type="hidden" name="verification" value="">
                            <span class="type_question">
                                @if ($question->type == 'bouton_radio')
                                    Bouton radio
                                @elseif($question->type == 'input')
                                    Input
                                @elseif($question->type == 'zone_texte')
                                    Zone de texte
                                @elseif($question->type == 'checkbox')
                                    Checkbox
                                @endif
                            </span>
                            <label for="nom_question">Nom</label>
                            @if(($question->type == 'checkbox') || ($question->type == 'bouton_radio'))
                                <input type="text" name="nom_question{{$z}}" class="nom_question multiple" value="{{$question->nom}}">
                                <table>
                                    <tr>
                                        <th>Nom</th>
                                        <th>Montant à ajouter</th>
                                    </tr>
                                    <?php $nbr_reponse = 0;
                                    $reponses = $question->section;
                                    ?>

                                    @foreach($reponses as $reponse)
                                        <?php $nbr_reponse++; ?>
                                        <tr>
                                            <td><input type="text" class="reponse-multiple{{$question->id}} selection" data-etapeid="{{$etape->id}}" name="reponse_multiple{{$nbr_reponse}}{{$question->id}}" value="{{$reponse->information}}"></td>
                                            <td><input type="number" class="montant-ajouter" name="montant_ajouter{{$nbr_reponse}}{{$question->id}}" value="{{$reponse->montant}}"></td>
                                            <td><a href="javascript:void(0)" class="reponse{{$nbr_reponse}}{{$question->id}} supp_reponse" onclick="supprimer_reponse({{$nbr_reponse}}{{$question->id}})">-</a></td>
                                            <input type="hidden" class="section-id" name="section-id{{$nbr_reponse}}{{$question->id}}" value="{{$reponse->id}}">
                                        </tr>
                                        <?php $y++; ?>
                                    @endforeach
                                </table>
                                <input type="hidden" value="{{$nbr_reponse}}" name="nbr_reponse{{$question->id}}" class="nbr_reponse{{$question->id}} reponse_info">
                                <a href="javascript:void(0)" id="ajouter_champs" class="ajouter_information">Ajouter</a>
                            @else
                                <input type="text" name="nom_question{{$z}}" class="nom_question" value="{{$question->nom}}">
                            @endif
                            <div class="affichersi" data-etapeid="{{$etape->id}}">
                                <label>Logique conditionnelle</label>
                                <input type="checkbox" id="condition" name="condition-verification{{$z}}" class="condition" {{$question->conditionParent == "" ? "" : "checked"}} ><br>
                                <div class="wrapper-condition {{$question->conditionParent == "" ? "" : "active"}}">
                                    <label>Afficher si</label>
                                    <select class="afficher_question" name="afficher_question{{$z}}">
                                        @if ($question->conditionParent == "")
                                            <option value=""></option>
                                        @endif
                                        @foreach($questions as $question_affiche)
                                            @if (($question_affiche->type === "checkbox") || (($question_affiche->type === "bouton_radio")))
                                                <option value="{{$question_affiche->id}}" {{$question_affiche->id == $question->conditionParent ? "selected" : ""}}>{{$question_affiche->nom}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                    <span>EST</span>
                                    <select class="afficher_reponse" name="afficher_reponse{{$z}}">
                                        @foreach(DB::table('sections')->where('question_id', '=', $question->conditionParent)->get() as $ma_section)
                                            <option value="{{$ma_section->id}}" {{$ma_section->id == $question->conditionSection ? "selected" : ""}}>{{$ma_section->information}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <a href="javascript:void(0)" data-questionnbr="{{$z}}" data-questionid="{{$question->id}}" class="supprimer_question">Supprimer la question</a>
                        </div>
                    @endforeach
                </div>
            @endforeach

        </div>
        <?php $id_reponse = DB::table('sections')->select('id')->latest('id')->first(); ?>
        <input type="hidden" class="section_id" name="section_id" value="{{$id_reponse->id ?? 0}}">
        <input type="hidden" class="dernier_id" name="dernier_id" value="{{$dernier_id}}">
        <input name="nombre_question" id="nombre_question" class="nom_question_final" type="hidden" value="{{$z}}">
        <div class="type-question">
            <div class="ajouter_question_btn">
                <span>Ajouter une question : </span>
                <a href="JavaScript:void(0)" class="bouton-question" id="checkbox">Checkbox</a>
                <a href="JavaScript:void(0)" class="bouton-question" id="input">Input</a>
                <a href="JavaScript:void(0)" class="bouton-question" id="bouton_radio">Bouton radio</a>
                <a href="JavaScript:void(0)" class="bouton-question" id="zone_de_texte">Zone de texte</a>
            </div>
            <a href="JavaScript:void(0);" class="ajouter-etape">Ajouter étape</a>
            <button type="submit" class="button is-link modifier_formulaire">Modifier le formulaire</button>
        </div>
    </form>

    <form method="POST" action="/formulaire/{{ $formulaire->id }}">
        @method('DELETE')
        @csrf

        <div class="form-group">
            <button type="submit" class="button btn btn-danger">Supprimer le formulaire</button>
        </div>
    </form>

    <a href="../">Retour</a>

@stop

