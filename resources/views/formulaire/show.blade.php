@extends('layouts.app')
@section('content')

    <h1>Formulaires</h1>
    <form method="POST" action="/formulaire/updateActive/">
        @csrf
    <table class="liste_table">
        <thead>
            <tr>
                <th>ID Formulaire</th>
                <th>Nom</th>
                <th>Actif</th>
            </tr>
        </thead>
        <tbody>
            @foreach($formulaires as $formulaire)
                <tr>
                    <td><a href="/formulaire/{{$formulaire->id}}/edit">{{$formulaire->id}}</a></td>
                    <td>{{$formulaire->nom}}</td>
                    @if ($formulaire->isActive == 1)
                    <td><input id="radioForm" checked value="{{$formulaire->id}}" name="radioForm" type="radio"/></td>
                    @else
                    <td><input id="radioForm" value="{{$formulaire->id}}" name="radioForm" type="radio"/></td>
                    @endif
                </tr>
            @endforeach
    </tbody>

    </table>
    <button class="btn btn-primary">Modifier Actif</button>
    </form>
    <button class="buttonForm" onclick="window.location='{{url("formulaire/create")}}'">Ajouter un formulaire</button>

@stop

