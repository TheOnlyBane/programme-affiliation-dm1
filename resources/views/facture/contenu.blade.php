<head>
    <title>Facture</title>
</head>
<body>
    <h1>Elefen</h1>
    <h2>Facture pour {{$paiement->userNom}}</h2>
    <div>
        <p>{{$paiement->userNom}}</p>
        <p>{{$paiement->userAdresse}}</p>
        @if ($paiement->userApp != null)
        <p>App. {{$paiement->userApp}}</p>
        @endif
        <p>{{$paiement->userCodePostal}}</p>
        <p>{{$paiement->userProvince}}</p>
        <p>{{$paiement->userTelephone}}</p>
        <p>{{$paiement->userEmail}}</p>
    </div>
    <table>
        <tr>
            <th>Projet</th>
            <th>Date</th>
            <th>Montant</th>
        </tr>
        <td>{{$paiement->projet->nom}}</td>
        <td>{{$paiement->date}}</td>     
        <td>{{$paiement->montant}}</td>     
    </table>
    <h3>Ancien Total: {{$paiement->montantProjet}}</h3>
    <h3>Nouveau Total: {{$paiement->montantProjet - $paiement->montant}}</h3>
</body>