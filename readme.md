# Table des matières

* Informations générales
* Technologies
* Équipe
* Prérequis
* Guide Installation

# Informations générales

Cette application est utilisée afin de créer un formulaire de soumission. L’administrateur pourra créer et personnaliser son propre formulaire de soumission afin que le membre affilié puisse partager un lien afin de remplir le formulaire créé par l’administrateur. L’administrateur pourra également ajouter des membres affiliés et ajouter des projets afin d’avoir une liste de ses projets et de ses paiements.


# Technologies utilisées

PHP 7.3.8
XAMPP 3.2.4
Gitlab
Laravel 6.4.0
PhpStorm 11.0.3

# Membre d'équipe

Anthony Girard

Jérémy Schneider


# Prérequis

- [ ] Avoir un logiciel d'archivage (Winrar, 7-Zip, etc)
- [ ] Une connexion Internet


# Guide d'installation du projet complet

## 1. Aller chercher le projet.

Il y a deux méthodes possibles pour aller chercher le projet.


**Méthode par Git**

Aller sur le site de Git au https://git-scm.com

Télécharger la dernière version disponible
(Optionel). Vous pouvez aussi cliquer directement sur le lien suivant
https://git-scm.com/download/win

Installer le logiciel avec les options par défaut
Créer un dossier à la racine du C. ex.: C:\Projet\
Ouvrir GitBash et entrer la commande suivante
`git clone https://gitlab.com/TheOnlyBane/programme-affiliation-dm1.git`




**Méthode par Internet**

Aller sur le site https://gitlab.com/TheOnlyBane/programme-affiliation-dm1

Télécharger l'archive en .zip
Créer un dossier à la racine du C. ex.: C:\Projet\
Extraire l'archive dans le dossier créé.




## 2. Installer XAMPP

Aller dans le dossier C:\Projet\programme-affiliation-dm1\Installation.
Extraire l'archive Installation.zip dans le dossier
Installer XAMPP à partir du fichier suivant : *1-XAMPP.exe*

Utiliser les options par défaut




## 3. Installer Composer

Aller dans le dossier C:\Projet\programme-affiliation-dm1\Installation.
Installer Composer à partir du fichier suivant : *2-Composer.exe*

Utiliser les options par défaut




## 4. Installer NPM

Aller dans le dossier C:\Projet\programme-affiliation-dm1\Installation.
Installer NPM à partir du fichier suivant : *3-NPM.exe*

Cocher l'option d'ajout dans le path (variables d'environnement)




## 5. Installer les dépendances de Composer

Aller dans le dossier C:\Projet\programme-affiliation-dm1\
Ouvrir une invite de commande dans ce dossier en entrant cmd dans la barre d'adresse de l'explorateur de fichier.

Sinon, dans le menu démarrer, rechercher cmd ou invite de commande
Entrer la commande suivante : `cd C:\Projet\programme-affiliation-dm1\`





Entrer la commande suivante : `composer install`



## 6. Installer les dépendances de NPM

Ouvrir une nouvelle invite de commande dans le dossier C:\Projet\programme-affiliation-dm1\
Lancer la commande suivante : `npm install`



## 7. Démarrer les services de Apache et MySQL

Dans le panneau de contrôle de XAMPP démarrer les services à l'aide des boutons d'actions
Sur la ligne MySQL, cliquer sur [Admin]
Cliquer sur le bouton 'nouvelle base de données' en haut à gauche
Entrer le nom de la base de données (Affiliation)
Changer l'encodage de texte pour 'utf8mb4_general_ci'
Cliquer sur [Créer]


## 8. Préparer le fichier d'environnement

Dans la même invite de commande entrer la commande suivante :
`copy .env.example .env`

Si vous avez changé le nom de la base de données:

Ouvrir le fichier .env à l'aide d'un éditeur de texte
Changer la ligne `DB_DATABASE=laravel` pour `DB_DATABASE=Affiliation` 



## 9. Créer la clé d'encryption

Dans l'invite de commande entrer la commande suivante:
`php artisan key:generate`


## 10. Faire la migration de la base de données

Dans la même invite de commande entrer la commande suivante:
`php artisan migrate`



## 11. Populer la base de données

Dans la même invite de commande entrer :
`composer dump-autoload` suivi de 
`php artisan db:seed`



## 12. Configuration de Apache

Dans le panneau de contrôle de XAMPP, sur la ligne de Apache, cliquer sur Config puis Apache (httpd.conf).
Changer les lignes
`DocumentRoot "C:/xampp/htdocs"` par `DocumentRoot "C:\Projet\programme-affiliation-dm1\public"`
et `<Directory "C:/xampp/htdocs">` par `<Directory "C:\Projet\programme-affiliation-dm1\public">`.
Redémarrer Apache (Stop, Start).












