<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use App\Affilie;
use App\Projet;
use App\User;
use App\Provenance;

class ProjetControllerTest extends TestCase
{

    use RefreshDatabase;
    //use WithoutMiddleware;

    public function setup() : void {
        parent::setup();
        $this->seed('AffilieTableSeeder');
        $this->seed('UsersTableSeeder');
        self::loginWithFakeUser();
    }

    private function loginWithFakeUser(){
        $user = new User([
            'id' =>1,
            'name' => 'Test User',
            'password'=>'test123',
            'email'=> 'fakeuser@user.com',
            'telephone' => '8191234567',
            'adresse'=> 'une adresse',
            'codePostal' => 'J2C4FF',
            'isAdmin'=>1,
            'provenance_id'=> Provenance::all()->first()->id,
            'affilie_id'=> Affilie::all()->first()->id
        ]);
        $this->actingAs($user);
    }

    // Test Route

    public function test_route_index(){
        $response = $this->get('projet');
        $response->assertViewHas('projets');
        $response->assertStatus(200);
        $projets = $response->original->getData()['projets'];
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $projets);
    }

    public function test_route_edit(){
        $this->seed('ProjetTableSeeder');
        $projet = Projet::all()->first();
        $response = $this->get('projet/'.$projet->id.'/edit');
        $response->assertViewHas('affilies');
        $response->assertStatus(200);
    }

    public function test_route_create(){
        $response = $this->get('projet/create');
        $response->assertViewHas('affilies');
        $response->assertStatus(200);
    }

    // Test CRUD
    public function test_creer_un_projet_avec_bonne_donnees(){
        $this->withoutExceptionHandling();

        $affilie = Affilie::all()->first();

        $projet = [
            'type'                  => 'reccurent',
            'nom'                   => 'EST-001',
            'date'                  => '2019-10-02',
            'status'                => 'en cours',
            'commission'            => 10,
            'montantTotal'          => 10000,
            'affilie'               => $affilie->id
        ];

        $reponse = $this->post('projet', $projet);
        $reponse->assertStatus(302);
        $this->assertDatabaseHas('projets', [
            'type'                  => 'reccurent',
            'nom'                   => 'EST-001',
            'date'                  => '2019-10-02',
            'status'                => 'en cours',
            'commissionPourcentage' => 10,
            'montantTotal'          => 10000,
            'montantActuel'         => 10000,
            'affilie_id'            => $affilie->id
        ]);
    }

    public function test_creer_un_projet_avec_donnees_null() {
        $affilie = Affilie::all()->first();

        $projet = [
            'type'                  => null,
            'nom'                   => null,
            'date'                  => null,
            'status'                => null,
            'commission'            => null,
            'montantTotal'          => null,
            'affilie'               => null
        ];

        $reponse = $this->post('projet', $projet);

        $reponse->assertSessionHasErrors([
            'nom',
            'type',
            'date',
            'status',
            'commission',
            'montantTotal',
            'affilie'
        ]);
    }

    public function test_creer_un_projet_cas_limite_max_fonctionnel(){
        $this->withoutExceptionHandling();

        $affilie = Affilie::all()->first();

        $projet = [
            'type'                  => 'qwertyuiopqwertyuiopqwertyuiopwertyuiopêrtyuiodfgy',
            'nom'                   => 'EST-150111111111111111111111111111111111111111111y',
            'date'                  => '2020-05-05',
            'status'                => 'finalpseeeeeeeeeeeeeeeeee',
            'commission'            => 15,
            'montantTotal'          => 20000,
            'affilie'               => $affilie->id
        ];

        $reponse = $this->post('projet', $projet);
        $reponse->assertStatus(302);
        $this->assertDatabaseHas('projets', [
            'type'                  => 'qwertyuiopqwertyuiopqwertyuiopwertyuiopêrtyuiodfgy',
            'nom'                   => 'EST-150111111111111111111111111111111111111111111y',
            'date'                  => '2020-05-05',
            'status'                => 'finalpseeeeeeeeeeeeeeeeee',
            'commissionPourcentage' => 15,
            'montantTotal'          => 20000,
            'montantActuel'         => 20000,
            'affilie_id'            => $affilie->id
        ]);
    }

    public function test_creer_un_projet_cas_limite_max_non_fonctionnel(){
        $affilie = Affilie::all()->first();

        $projet = [
            'type'                  => 'qwertyuiopqwertyuiopqwertyuiopwertyuiopêrtyuiodfgyq',
            'nom'                   => 'EST-150111111111111111111111111111111111111111111yq',
            'date'                  => '2020-05-05',
            'status'                => 'finalpseeeeeeeeeeeeeeeeeeq',
            'commission'            => 15,
            'montantTotal'          => 20000,
            'affilie'               => $affilie->id
        ];

        $reponse = $this->post('projet', $projet);

        $reponse->assertSessionHasErrors([
            'nom',
            'type',
            'status',
        ]);
    }

    public function test_creer_un_projet_cas_limite_min_fonctionnel(){
        $this->withoutExceptionHandling();

        $affilie = Affilie::all()->first();

        $projet = [
            'type'                  => 'qwe',
            'nom'                   => 'EST',
            'date'                  => '2020-05-05',
            'status'                => 'en cours',
            'commission'            => 15,
            'montantTotal'          => 20000,
            'affilie'               => $affilie->id
        ];

        $reponse = $this->post('projet', $projet);
        $reponse->assertStatus(302);
        $this->assertDatabaseHas('projets', [
            'type'                  => 'qwe',
            'nom'                   => 'EST',
            'date'                  => '2020-05-05',
            'status'                => 'en cours',
            'commissionPourcentage' => 15,
            'montantTotal'          => 20000,
            'montantActuel'         => 20000,
            'affilie_id'            => $affilie->id
        ]);
    }

    public function test_creer_un_project_cas_limite_min_non_fonctionnel(){
        $affilie = Affilie::all()->first();

        $projet = [
            'type'                  => 'qw',
            'nom'                   => 'ES',
            'date'                  => '2020-05-05',
            'status'                => 'fi',
            'commission'            => 15,
            'montantTotal'          => 20000,
            'affilie'               => $affilie->id
        ];

        $reponse = $this->post('projet', $projet);

        $reponse->assertSessionHasErrors([
            'nom',
            'type',
            'status',
        ]);
    }

    public function test_creer_un_projet_avec_nombre_negatif(){
        $affilie = Affilie::all()->first();

        $projet = [
            'type'                  => 'qws',
            'nom'                   => 'ESs',
            'date'                  => '2020-05-05',
            'status'                => 'fin',
            'commission'            => -15,
            'montantTotal'          => -20000,
            'affilie'               => $affilie->id
        ];

        $reponse = $this->post('projet', $projet);

        $reponse->assertSessionHasErrors([
            'commission',
            'montantTotal',
        ]);
    }

    public function test_creer_un_projet_avec_fausse_date(){
        $affilie = Affilie::all()->first();

        $projet = [
            'type'                  => 'qws',
            'nom'                   => 'ESs',
            'date'                  => 'test',
            'status'                => 'fin',
            'commission'            => 15,
            'montantTotal'          => 20000,
            'affilie'               => $affilie->id
        ];

        $reponse = $this->post('projet', $projet);

        $reponse->assertSessionHasErrors([
            'date',
        ]);
    }

    public function test_modifier_un_projet_avec_bonne_donnees(){
        $this->seed('ProjetTableSeeder');
        $projet = Projet::all()->last();
        $affilie = Affilie::all()->last();

        $data = [
            'type'                  => 'fixe',
            'nom'                   => 'EST-1501',
            'date'                  => '2020-05-05',
            'status'                => 'en_cours',
            'commission'            => 15,
            'montantTotal'          => 20000,
            'affilie'               => $affilie->id
        ];

        $reponse = $this->put('projet/'.$projet->id, $data);

        $projetModifie = Projet::find($projet->id);

        $this->assertEquals($data["type"], $projetModifie->type);
        $this->assertEquals($data["nom"], $projetModifie->nom);
        $this->assertEquals($data["date"], $projetModifie->date);
        $this->assertEquals($data["status"], $projetModifie->status);
        $this->assertEquals($data["commission"], $projetModifie->commissionPourcentage);
        $this->assertEquals($data["montantTotal"], $projetModifie->montantTotal);
    }

    public function test_modifier_un_projet_cas_limite_max_fonctionnel(){
        $this->seed('ProjetTableSeeder');
        $projet = Projet::all()->first();
        $affilie = Affilie::all()->last();

        $data = [
            'type'                  => 'qwertyuiopqwertyuiopqwertyuiopwertyuiopêrtyuiodfgy',
            'nom'                   => 'EST-150111111111111111111111111111111111111111111y',
            'date'                  => '2020-05-05',
            'status'                => 'finaliseeeeeeeeeeeeeeeeey',
            'commission'            => 15,
            'montantTotal'          => 20000,
            'affilie'               => $affilie->id
        ];

        $reponse = $this->put('projet/'.$projet->id, $data);

        $projetModifie = Projet::find($projet->id);


        $this->assertEquals($data["type"], $projetModifie->type);
        $this->assertEquals($data["nom"], $projetModifie->nom);
        $this->assertEquals($data["date"], $projetModifie->date);
        $this->assertEquals($data["status"], $projetModifie->status);
        $this->assertEquals($data["commission"], $projetModifie->commissionPourcentage);
        $this->assertEquals($data["montantTotal"], $projetModifie->montantTotal);
    }

    public function test_modifier_un_projet_cas_limite_max_non_fonctionnel(){
        $this->seed('ProjetTableSeeder');
        $projet = Projet::all()->first();
        $affilie = Affilie::all()->last();

        $data = [
            'type'                  => 'qwertyuiopqwertyuiopqwertyuiopwertyuiopêrtyuiodfgys',
            'nom'                   => 'EST-150111111111111111111111111111111111111111111ys',
            'date'                  => '2020-05-05',
            'status'                => 'finaliseeeeeeeeeeeeeeeeeys',
            'commission'            => 15,
            'montantTotal'          => 20000,
            'affilie'               => $affilie->id
        ];

        $reponse = $this->put('projet/'.$projet->id, $data);

        $projetModifie = Projet::find($projet->id);

        $reponse->assertSessionHasErrors([
            'nom',
            'type',
            'status',
        ]);
    }

    public function test_modifier_un_project_cas_limite_min_fonctionnel(){
        $this->seed('ProjetTableSeeder');
        $projet = Projet::all()->first();
        $affilie = Affilie::all()->last();

        $data = [
            'type'                  => 'tes',
            'nom'                   => 'tes',
            'date'                  => '2020-05-05',
            'status'                => 'tes',
            'commission'            => 15,
            'montantTotal'          => 20000,
            'affilie'               => $affilie->id
        ];

        $reponse = $this->put('projet/'.$projet->id, $data);

        $projetModifie = Projet::find($projet->id);


        $this->assertEquals($data["type"], $projetModifie->type);
        $this->assertEquals($data["nom"], $projetModifie->nom);
        $this->assertEquals($data["date"], $projetModifie->date);
        $this->assertEquals($data["status"], $projetModifie->status);
        $this->assertEquals($data["commission"], $projetModifie->commissionPourcentage);
        $this->assertEquals($data["montantTotal"], $projetModifie->montantTotal);
    }

    public function test_modifier_un_projet_cas_limite_min_non_fonctionnel() {
        $this->seed('ProjetTableSeeder');
        $projet = Projet::all()->first();
        $affilie = Affilie::all()->last();

        $data = [
            'type'                  => 'ee',
            'nom'                   => 'ee',
            'date'                  => '2020-05-05',
            'status'                => 'ee',
            'commission'            => 15,
            'montantTotal'          => 20000,
            'affilie'               => $affilie->id
        ];

        $reponse = $this->put('projet/'.$projet->id, $data);

        $projetModifie = Projet::find($projet->id);

        $reponse->assertSessionHasErrors([
            'nom',
            'type',
            'status',
        ]);
    }

    public function test_modifier_un_projet_avec_nombre_negatif(){
        $this->seed('ProjetTableSeeder');
        $projet = Projet::all()->first();
        $affilie = Affilie::all()->last();

        $data = [
            'type'                  => 'Recurrent',
            'nom'                   => 'EST-1501',
            'date'                  => '2020-05-05',
            'status'                => 'finalise',
            'commission'            => -1,
            'montantTotal'          => -1,
            'affilie'               => $affilie->id
        ];

        $reponse = $this->put('projet/'.$projet->id, $data);

        $projetModifie = Projet::find($projet->id);

        $reponse->assertSessionHasErrors([
            'commission',
            'montantTotal',
        ]);
    }

    public function test_modifier_un_projet_avec_fausse_date(){
        $this->seed('ProjetTableSeeder');
        $projet = Projet::all()->first();
        $affilie = Affilie::all()->last();

        $data = [
            'type'                  => 'Recurrent',
            'nom'                   => 'EST-1501',
            'date'                  => 'test',
            'status'                => 'finalise',
            'commission'            => -1,
            'montantTotal'          => -1,
            'affilie'               => $affilie->id
        ];

        $reponse = $this->put('projet/'.$projet->id, $data);

        $projetModifie = Projet::find($projet->id);

        $reponse->assertSessionHasErrors([
            'date',
        ]);
    }

    public function test_modifie_un_projet_avec_donnees_null() {
        $this->seed('ProjetTableSeeder');

        $projet = Projet::all()->first();
        $affilie = Affilie::all()->last();

        $data = [
            'type'                  => null,
            'nom'                   => null,
            'date'                  => null,
            'status'                => null,
            'commission'            => null,
            'montantTotal'          => null,
            'affilie'               => null,
        ];

        $reponse = $this->put('projet/'.$projet->id, $data);
        $reponse->assertSessionHasErrors([
            'nom',
            'type',
            'date',
            'status',
            'commission',
            'montantTotal',
            'affilie'
        ]);
    }

    public function test_supprimer_un_projet_avec_bonne_donnees(){
        $this->seed('ProjetTableSeeder');

        $projet = Projet::all()->first();
        $reponse = $this->delete('projet/'.$projet->id);
        $this->assertDatabaseMissing('projets', ['id'=>$projet->id]);
    }

    public function test_supprimer_un_projet_avec_donne_errone(){
        $reponse = $this->delete('projet/454');
        $reponse->assertStatus(404);
    }
}

