<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Http\Requests\AffilieUpdateRequest;
use App\Http\Requests\AffilieStoreRequest;
use App\Affilie;
use App\Provenance;
use App\User;
class ControllerAffilieTest extends TestCase
{
    use RefreshDatabase;

    public function setup() : void{
        parent::setup();
        $this->seed('AffilieTableSeeder');
        $this->seed('UsersTableSeeder');
        $this->seed("ProjetTableSeeder");
        $this->seed('PaiementTableSeeder');
        self::loginWithFakeUser();

    }

    //! Méthodes privées
    private function loginWithFakeUser(){
        $user = new User([
            'id' =>1,
            'name' => 'Test User',
            'password'=>'test123',
            'email'=> 'fakeuser@user.com',
            'telephone' => '8191234567',
            'adresse'=> 'une adresse',
            'codePostal' => 'J2C4FF',
            'isAdmin'=>1,
            'provenance_id'=> Provenance::all()->first()->id,
            'affilie_id'=> Affilie::all()->first()->id
        ]);
        $this->actingAs($user);
    }

    //! Tests de vue
    public function test_route_index(){
        $response = $this->get('affilie');
        $response->assertViewHas('affilies');
        $response->assertStatus(200);
        $affilies = $response->original->getData()['affilies'];
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $affilies);
    }

    public function test_route_create(){
        $response = $this->get('affilie/create');
        $response->assertViewHas('Provenances');
        $response->assertStatus(200);
    }

    public function test_route_edit(){
        $affilie = Affilie::all()->first();
        $response = $this->get('affilie/'.$affilie->id.'/edit');
        $response->assertViewHas('affilie');
        $response->assertStatus(200);
    }

    public function test_route_edit_user(){
        $response = $this->get('affilie/edit');
        $response->assertStatus(200);
    }

    //! Tests d'intégration des CRUDs

    public function test_ajouter_affilie_normal(){
        $affilie = factory(Affilie::class)->create();
        $user = factory(User::class)->create(['affilie_id' => $affilie->id]);
        $response = $this->post('affilie', $affilie->toArray());
        $response->assertStatus(302);
        $this->assertDatabaseHas('Affilies',[
            'nomEntreprise' => $affilie->nomEntreprise,
        ]);
    }

    public function test_ajouter_affilie_donnees_erronees(){
        $affilie = [
            'name' => null,
            'email' => null,
            'password' => null,
            'telephone' => null,
            'adresse' => null,
            'codePostal' => null,
            'ProvenanceID' => null,
            'solde'=>"allo",
            'commissionReccurent'=>null,
            'commissionSimple'=>null,
            'numTPS'=>null,
            'numTVQ'=>null,
            'nomEntreprise'=>null,
            'numEntreprise'=>null,
            'numTelEntreprise'=>null
        ];
        $response = $this->post('affilie', $affilie);
        $response->assertSessionHasErrors([
            'name',
            'email',
            'password',
            'telephone',
            'adresse',
            'codePostal',
            'ProvenanceID',
            'solde',
            'commissionReccurent',
            'commissionSimple',
            'numTPS',
            'numTVQ',
            'nomEntreprise',
            'numEntreprise',
            'numTelEntreprise'
        ]);
    }

    public function test_ajouter_affilie_donnees_limite_minimum_fonctionnel(){
        $affilie = [
            'name' => '123',
            'email' => 'test3@user.com',
            'password' => '12345678',
            'telephone' => '1234567890',
            'adresse' => 'Adresse Test',
            'codePostal' => 'J3C444',
            'ProvenanceID' => Provenance::all()->first()->id,
            'solde'=>0,
            'commissionReccurent'=>5,
            'commissionSimple'=>7,
            'numTPS'=>'1234-5678',
            'numTVQ'=>'1234-5678',
            'nomEntreprise'=>'Tes',
            'numEntreprise'=>'123',
            'numTelEntreprise'=>'1234567890'
        ];
        $response = $this->post('affilie', $affilie);
        $response->assertStatus(302);
        $this->assertDatabaseHas('Affilies',[
            'nomEntreprise' => 'Tes',
        ]);
    }

    // Impossible de tester les mots de passe à cause du Hash
    // Les courriels ne sont pas testés à cause de la clause unique
    public function test_ajouter_affilie_donnees_limite_minimum_erronees(){
        $affilie = [
            'name' => 'te',
            'email' => 'test3@user.com',
            'password' => '1234567',
            'telephone' => '123456789',
            'adresse' => 'Adresse Test',
            'codePostal' => 'J3C444',
            'ProvenanceID' => Provenance::all()->first()->id,
            'solde'=>-1,
            'commissionReccurent'=>5,
            'commissionSimple'=>7,
            'numTPS'=>'1234-5678',
            'numTVQ'=>'1234-5678',
            'nomEntreprise'=>'Te',
            'numEntreprise'=>'12',
            'numTelEntreprise'=>'123456789'
        ];
        $response = $this->post('affilie', $affilie);
        $response->assertSessionHasErrors([
            'name',
            'telephone',
            'nomEntreprise',
            'numEntreprise',
            'numTelEntreprise',
            'password',
            'solde'
        ]);
    }

    public function test_ajouter_affilie_donnees_limite_maximum_fonctionnel(){
        $affilie = [
            'name' => 'xtrmhydncydfyuauwincxtrmhydncydfyuauwinc1234567890',
            'email' => 'test3@user.com',
            'password' => '12345678',
            'telephone' => '1234567890123456789012345',
            'adresse' => 'xtrmhydncydfyuauwincxtrmhydncydfyuauwinc1234567890xtrmhydncydfyuauwincxtrmhydncydfyuauwinc1234567890',
            'codePostal' => 'J3C444',
            'ProvenanceID' => Provenance::all()->first()->id,
            'solde'=>240,
            'commissionReccurent'=>5,
            'commissionSimple'=>7,
            'numTPS'=>'xtrmhydncydfyuauwincxtrmhydncydfyuauwinc1234567890',
            'numTVQ'=>'xtrmhydncydfyuauwincxtrmhydncydfyuauwinc1234567890',
            'nomEntreprise'=>'xtrmhydncydfyuauwincxtrmhydncydfyuauwinc1234567890',
            'numEntreprise'=>'xtrmhydncydfyuauwincxtrmhydncydfyuauwinc1234567890',
            'numTelEntreprise'=>'1234567890123456789012345'
        ];
        $response = $this->post('affilie', $affilie);
        $response->assertStatus(302);
        $this->assertDatabaseHas('Affilies',[
            'nomEntreprise' => 'xtrmhydncydfyuauwincxtrmhydncydfyuauwinc1234567890',
        ]);
    }

    // Impossible de tester les mots de passe à cause du Hash
    // Les courriels ne sont pas testés à cause de la clause unique
    public function test_ajouter_affilie_donnees_limite_maximum_erronees(){
        $affilie = [
            'name' => 'xtrmhydncydfyuauwincxtrmhydncydfyuauwinc12345678901',
            'email' => 'test3@user.com',
            'password' => '12345678',
            'telephone' => '12345678901234567890123456',
            'adresse' => 'xtrmhydncydfyuauwincxtrmhydncydfyuauwinc1234567890xtrmhydncydfyuauwincxtrmhydncydfyuauwinc12345678901',
            'app' => '123456789012345678901234512345678901234567890123456',
            'codePostal' => 'xtrmhydncydfyuauwincxtrmhydncydfyuauwinc12345678901',
            'ProvenanceID' => Provenance::all()->first()->id,
            'solde'=>240,
            'commissionReccurent'=>5,
            'commissionSimple'=>7,
            'numTPS'=>'xtrmhydncydfyuauwincxtrmhydncydfyuauwinc12345678901',
            'numTVQ'=>'xtrmhydncydfyuauwincxtrmhydncydfyuauwinc12345678901',
            'nomEntreprise'=>'xtrmhydncydfyuauwincxtrmhydncydfyuauwinc12345678901',
            'numEntreprise'=>'xtrmhydncydfyuauwincxtrmhydncydfyuauwinc12345678901',
            'numTelEntreprise'=>'123456789012345678901234512345678901234567890123456'
        ];
        $response = $this->post('affilie', $affilie);
        $response->assertSessionHasErrors([
            'name',
            'telephone',
            'adresse',
            'app',
            'codePostal',
            'numTPS',
            'numTVQ',
            'nomEntreprise',
            'numEntreprise',
            'numTelEntreprise'
        ]);
    }

    public function test_ajouter_affilie_meme_courriel(){
        $affilie = factory(Affilie::class)->create();
        $user = factory(User::class)->create([
            'affilie_id' => $affilie->id
        ]);
        $response = $this->post('affilie', array_merge($affilie->toArray(), $user->toArray()));
        $response->assertSessionHasErrors([
            'email'
        ]);
    }
    
    public function test_ajouter_affilie_courriel_invalide(){
        $affilie = factory(Affilie::class)->create();
        $user = factory(User::class)->create([
            'affilie_id' => $affilie->id,
            'email' => 'adminadmin.com'
        ]);
        $affilie->user->email = 'adminadmin.com';
        $response = $this->post('affilie', array_merge($affilie->toArray(), $user->toArray()));
        $response->assertSessionHasErrors([
            'email'
        ]);
    }

    public function test_modifier_affilie_normal(){
        $affilie = Affilie::all()->first();
        $affilie->user->name = $affilie->user->name.'test';
        $affilie->user->telephone = $affilie->user->telephone.'123';
        $affilie->user->adresse = $affilie->user->adresse.'test';
        $affilie->user->codePostal = $affilie->user->codePostal.'test';
        $affilie->user->provenance_id = $affilie->user->provenance_id;
        $affilie->solde = $affilie->solde+1;
        $affilie->commissionReccurent = $affilie->commissionReccurent+1;
        $affilie->commissionSimple = $affilie->commissionSimple+1;
        $affilie->numTPS = $affilie->numTPS.'test';
        $affilie->numTVQ = $affilie->numTVQ.'test';
        $affilie->nomEntreprise = $affilie->nomEntreprise.'test';
        $affilie->numEntreprise = $affilie->numEntreprise.'test';
        $affilie->numTelEntreprise = $affilie->numTelEntreprise.'test';
        $data = [
            'name' => $affilie->user->name,
            'telephone' => $affilie->user->telephone,
            'adresse' => $affilie->user->adresse,
            'codePostal' => $affilie->user->codePostal,
            'ProvenanceID' => $affilie->user->provenance_id,
            'solde'=>$affilie->solde,
            'commissionReccurent'=>$affilie->commissionReccurent,
            'commissionSimple'=>$affilie->commissionSimple,
            'numTPS'=>$affilie->numTPS,
            'numTVQ'=>$affilie->numTVQ,
            'nomEntreprise'=>$affilie->nomEntreprise,
            'numEntreprise'=>$affilie->numEntreprise,
            'numTelEntreprise'=>$affilie->numTelEntreprise
        ];
        $response = $this->put('affilie/'.$affilie->id, $data);
        $response->assertStatus(302);
        $affilieModifie = Affilie::find($affilie->id);
        $this->assertEquals($affilie->user->name, $affilieModifie->user->name);
        $this->assertEquals($affilie->user->password, $affilieModifie->user->password);
        $this->assertEquals($affilie->user->telephone, $affilieModifie->user->telephone);
        $this->assertEquals($affilie->user->adresse, $affilieModifie->user->adresse);
        $this->assertEquals($affilie->user->codePostal, $affilieModifie->user->codePostal);
        $this->assertEquals($affilie->user->provenance_id, $affilieModifie->user->provenance_id);
        $this->assertEquals($affilie->solde, $affilieModifie->solde);
        $this->assertEquals($affilie->commissionReccurent, $affilieModifie->commissionReccurent);
        $this->assertEquals($affilie->commissionSimple, $affilieModifie->commissionSimple);
        $this->assertEquals($affilie->numTPS, $affilieModifie->numTPS);
        $this->assertEquals($affilie->numTVQ, $affilieModifie->numTVQ);
        $this->assertEquals($affilie->nomEntreprise, $affilieModifie->nomEntreprise);
        $this->assertEquals($affilie->numEntreprise, $affilieModifie->numEntreprise);
        $this->assertEquals($affilie->numTelEntreprise, $affilieModifie->numTelEntreprise);
        
    }

    public function test_modifier_affilie_errone(){
        $affilie = Affilie::all()->first();
        $affilie->user->name = '12';
        $affilie->user->telephone = '13';
        $affilie->user->adresse = null;
        $affilie->user->codePostal = null;
        $affilie->user->provenance_id = null;
        $affilie->commissionReccurent = null;
        $affilie->commissionSimple = null;
        $affilie->solde = -1;
        $affilie->numTPS = null;
        $affilie->numTVQ = null;
        $affilie->nomEntreprise = '12';
        $affilie->numEntreprise = '12';
        $affilie->numTelEntreprise = '12345678';
        $data = [
            'name' => $affilie->user->name,
            'telephone' => $affilie->user->telephone,
            'adresse' => $affilie->user->adresse,
            'codePostal' => $affilie->user->codePostal,
            'ProvenanceID' => $affilie->user->provenance_id,
            'commissionReccurent'=>$affilie->commissionReccurent,
            'commissionSimple'=>$affilie->commissionSimple,
            'solde' => $affilie->solde,
            'numTPS'=>$affilie->numTPS,
            'numTVQ'=>$affilie->numTVQ,
            'nomEntreprise'=>$affilie->nomEntreprise,
            'numEntreprise'=>$affilie->numEntreprise,
            'numTelEntreprise'=>$affilie->numTelEntreprise
        ];
        $response = $this->put('affilie/'.$affilie->id, $data);
        $response->assertSessionHasErrors([
            'name',
            'telephone',
            'adresse',
            'codePostal',
            'ProvenanceID',
            'commissionReccurent',
            'commissionSimple',
            'numTPS',
            'numTVQ',
            'solde',
            'nomEntreprise',
            'numEntreprise',
            'numTelEntreprise'
        ]);
    }
    public function test_modifier_affilie_limite_minimum_fonctionnel(){
        $affilie = Affilie::all()->first();
        $affilie->user->name = '123';
        $affilie->user->password = "12345678";
        $affilie->user->telephone = '1234567890';
        $affilie->nomEntreprise = '123';
        $affilie->numEntreprise = '123';
        $affilie->numTelEntreprise = '1234567890';
        $affilie->solde = 0;
        $data = [
            'name' => $affilie->user->name,
            'password' => $affilie->user->password,
            'telephone' => $affilie->user->telephone,
            'adresse' => $affilie->user->adresse,
            'codePostal' => $affilie->user->codePostal,
            'ProvenanceID' => $affilie->user->provenance_id,
            'solde'=>$affilie->solde,
            'commissionReccurent'=>$affilie->commissionReccurent,
            'commissionSimple'=>$affilie->commissionSimple,
            'numTPS'=>$affilie->numTPS,
            'numTVQ'=>$affilie->numTVQ,
            'nomEntreprise'=>$affilie->nomEntreprise,
            'numEntreprise'=>$affilie->numEntreprise,
            'numTelEntreprise'=>$affilie->numTelEntreprise
        ];
        $response = $this->put('affilie/'.$affilie->id, $data);
        $response->assertStatus(302);
        $affilieModifie = Affilie::find($affilie->id);
        $this->assertTrue(Hash::check("12345678", $affilieModifie->user->password));
        $this->assertEquals($affilie->user->name, $affilieModifie->user->name);
        $this->assertEquals($affilie->user->telephone, $affilieModifie->user->telephone);
        $this->assertEquals($affilie->commissionReccurent, $affilieModifie->commissionReccurent);
        $this->assertEquals($affilie->commissionSimple, $affilieModifie->commissionSimple);
        $this->assertEquals($affilie->nomEntreprise, $affilieModifie->nomEntreprise);
        $this->assertEquals($affilie->numEntreprise, $affilieModifie->numEntreprise);
        $this->assertEquals($affilie->numTelEntreprise, $affilieModifie->numTelEntreprise);
        $this->assertEquals($affilie->solde, $affilieModifie->solde);
    }

    public function test_modifier_affilie_limite_minimum_erronee(){
        $affilie = Affilie::all()->first();
        $affilie->user->name = '12';
        $affilie->user->telephone = '123456789';
        $affilie->nomEntreprise = '12';
        $affilie->numEntreprise = '12';
        $affilie->numTelEntreprise = '123456789';
        $affilie->solde = -1;
        $data = [
            'name' => $affilie->user->name,
            'telephone' => $affilie->user->telephone,
            'adresse' => $affilie->user->adresse,
            'codePostal' => $affilie->user->codePostal,
            'ProvenanceID' => $affilie->user->provenance_id,
            'solde'=>$affilie->solde,
            'commissionReccurent'=>$affilie->commissionReccurent,
            'commissionSimple'=>$affilie->commissionSimple,
            'numTPS'=>$affilie->numTPS,
            'numTVQ'=>$affilie->numTVQ,
            'nomEntreprise'=>$affilie->nomEntreprise,
            'numEntreprise'=>$affilie->numEntreprise,
            'numTelEntreprise'=>$affilie->numTelEntreprise
        ];
        $response = $this->put('affilie/'.$affilie->id, $data);
        $response = $this->put('affilie/'.$affilie->id, $data);
        $response->assertSessionHasErrors([
            'name',
            'solde',
            'telephone',
            'nomEntreprise',
            'numEntreprise',
            'numTelEntreprise'
        ]);
    }

    public function test_modifier_affilie_limite_maximum_fonctionnel(){
        $affilie = Affilie::all()->first();
        $affilie->user->name = 'xtrmhydncydfyuauwincxtrmhydncydfyuauwinc1234567890';
        $affilie->user->telephone = '1234567890123456789012345';
        $affilie->nomEntreprise = 'xtrmhydncydfyuauwincxtrmhydncydfyuauwinc1234567890';
        $affilie->numEntreprise = 'xtrmhydncydfyuauwincxtrmhydncydfyuauwinc1234567890';
        $affilie->numTelEntreprise = 'xtrmhydncydfyuauwincxtrmhydncydfyuauwinc1234567890';
        $affilie->user->adresse = 'xtrmhydncydfyuauwinc'."xtrmhydncydfyuauwinc"."xtrmhydncydfyuauwinc"."xtrmhydncydfyuauwinc"."xtrmhydncydfyuauwinc";
        $affilie->user->codePostal = 'xtrmhydncydfyuauwincxtrmhydncydfyuauwinc1234567890';
        $affilie->user->app = 'xtrmhydncydfyuauwincxtrmhydncydfyuauwinc1234567890';
        $data = [
            'name' => $affilie->user->name,
            'telephone' => $affilie->user->telephone,
            'adresse' => $affilie->user->adresse,
            'codePostal' => $affilie->user->codePostal,
            'app' => $affilie->user->app,
            'ProvenanceID' => $affilie->user->provenance_id,
            'solde'=>$affilie->solde,
            'commissionReccurent'=>$affilie->commissionReccurent,
            'commissionSimple'=>$affilie->commissionSimple,
            'numTPS'=>$affilie->numTPS,
            'numTVQ'=>$affilie->numTVQ,
            'nomEntreprise'=>$affilie->nomEntreprise,
            'numEntreprise'=>$affilie->numEntreprise,
            'numTelEntreprise'=>$affilie->numTelEntreprise
        ];
        $response = $this->put('affilie/'.$affilie->id, $data);
        $response->assertStatus(302);
        $affilieModifie = Affilie::find($affilie->id);
        $this->assertEquals($affilie->user->name, $affilieModifie->user->name);
        $this->assertEquals($affilie->user->telephone, $affilieModifie->user->telephone);
        $this->assertEquals($affilie->commissionReccurent, $affilieModifie->commissionReccurent);
        $this->assertEquals($affilie->commissionSimple, $affilieModifie->commissionSimple);
        $this->assertEquals($affilie->nomEntreprise, $affilieModifie->nomEntreprise);
        $this->assertEquals($affilie->numEntreprise, $affilieModifie->numEntreprise);
        $this->assertEquals($affilie->numTelEntreprise, $affilieModifie->numTelEntreprise);
        $this->assertEquals($affilie->user->adresse, $affilieModifie->user->adresse);
        $this->assertEquals($affilie->user->codePostal, $affilieModifie->user->codePostal);
        $this->assertEquals($affilie->user->app, $affilieModifie->user->app);
    }

    public function test_modifier_affilie_limite_maximum_erronee(){
        $affilie = Affilie::all()->first();
        $affilie->user->name = 'xtrmhydncydfyuauwincxtrmhydncydfyuauwinc12345678901';
        $affilie->user->telephone = '12345678901234567890123456';
        $affilie->nomEntreprise = 'xtrmhydncydfyuauwincxtrmhydncydfyuauwinc12345678901';
        $affilie->numEntreprise = 'xtrmhydncydfyuauwincxtrmhydncydfyuauwinc12345678901';
        $affilie->numTelEntreprise = 'xtrmhydncydfyuauwincxtrmhydncydfyuauwinc12345678901';
        $affilie->user->adresse = 'xtrmhydncydfyuauwinc'."xtrmhydncydfyuauwinc"."xtrmhydncydfyuauwinc"."xtrmhydncydfyuauwinc"."xtrmhydncydfyuauwinc1";
        $affilie->user->codePostal = 'xtrmhydncydfyuauwincxtrmhydncydfyuauwinc12345678901';
        $affilie->user->app = 'xtrmhydncydfyuauwincxtrmhydncydfyuauwinc12345678901';
        $data = [
            'name' => $affilie->user->name,
            'telephone' => $affilie->user->telephone,
            'adresse' => $affilie->user->adresse,
            'codePostal' => $affilie->user->codePostal,
            'app' => $affilie->user->app,
            'ProvenanceID' => $affilie->user->provenance_id,
            'solde'=>$affilie->solde,
            'commissionReccurent'=>$affilie->commissionReccurent,
            'commissionSimple'=>$affilie->commissionSimple,
            'numTPS'=>$affilie->numTPS,
            'numTVQ'=>$affilie->numTVQ,
            'nomEntreprise'=>$affilie->nomEntreprise,
            'numEntreprise'=>$affilie->numEntreprise,
            'numTelEntreprise'=>$affilie->numTelEntreprise
        ];
        $response = $this->put('affilie/'.$affilie->id, $data);
        $response = $this->put('affilie/'.$affilie->id, $data);
        $response->assertSessionHasErrors([
            'name',
            'telephone',
            'nomEntreprise',
            'numEntreprise',
            'numTelEntreprise',
            'adresse',
            'codePostal',
            'app'
        ]);
    }

    public function test_modifier_utilisateur_mauvais_id(){
        $affilie = Affilie::all()->first();
        $affilie->user->name = "12345";
        $data = [
            'name' => $affilie->user->name,
            'telephone' => $affilie->user->telephone,
            'adresse' => $affilie->user->adresse,
            'codePostal' => $affilie->user->codePostal,
            'ProvenanceID' => $affilie->user->provenance_id,
            'solde'=>$affilie->solde,
            'commissionReccurent'=>$affilie->commissionReccurent,
            'commissionSimple'=>$affilie->commissionSimple,
            'numTPS'=>$affilie->numTPS,
            'numTVQ'=>$affilie->numTVQ,
            'nomEntreprise'=>$affilie->nomEntreprise,
            'numEntreprise'=>$affilie->numEntreprise,
            'numTelEntreprise'=>$affilie->numTelEntreprise
        ];
        $response = $this->patch('affilie/updateUser/12', $data);
        $response->assertStatus(404);
    }

    public function test_modifier_utilisateur_non_admin_normal(){
        $user = new User([
            'id' =>1,
            'name' => 'Test User',
            'password'=>'test123',
            'email'=> 'fakeuser@user.com',
            'telephone' => '8191234567',
            'adresse'=> 'une adresse',
            'codePostal' => 'J2C4FF',
            'isAdmin'=>0,
            'provenance_id'=> Provenance::all()->first()->id,
            'affilie_id'=> Affilie::all()->first()->id
        ]);
        $this->actingAs($user);
        $affilie = Affilie::all()->first();
        $affilie->user->name = "12345";
        $data = [
            'name' => $affilie->user->name,
            'telephone' => $affilie->user->telephone,
            'adresse' => $affilie->user->adresse,
            'codePostal' => $affilie->user->codePostal,
            'ProvenanceID' => $affilie->user->provenance_id,
            'solde'=>$affilie->solde,
            'commissionReccurent'=>$affilie->commissionReccurent,
            'commissionSimple'=>$affilie->commissionSimple,
            'numTPS'=>$affilie->numTPS,
            'numTVQ'=>$affilie->numTVQ,
            'nomEntreprise'=>$affilie->nomEntreprise,
            'numEntreprise'=>$affilie->numEntreprise,
            'numTelEntreprise'=>$affilie->numTelEntreprise
        ];
        $response = $this->patch('affilie/updateUser/'.$affilie->id, $data);
        $response->assertStatus(302);
        $affilieModifie = Affilie::find($affilie->id);
        $this->assertEquals($affilie->user->name, $affilieModifie->user->name);
    }
   
   
    public function test_supprimer_affilie_normal(){
        $this->withoutExceptionHandling();
        $affilie = Affilie::all()->first();
        $response = $this->delete('affilie/'.$affilie->id);
        $this->assertDatabaseMissing('Affilies',['id'=>$affilie->id]);
    }

    public function test_supprimer_affilie_errone(){
        $response = $this->delete('affilie/'.'240');
        $response->assertStatus(404);

    }
}
