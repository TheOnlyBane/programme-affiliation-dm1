<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use App\Formulaire;
use App\Etape;


class ControllerEtapeTest extends TestCase
{

    use RefreshDatabase;
    use WithoutMiddleware;

    public function setup() : void {
        parent::setup();
        //Un formulaire doit être créer pour ajouter des étapes
        $this->seed('FormulaireTableSeeder');
    }

    public function test_creer_une_etape_avec_bonnes_donnees() {
        $this->withoutExceptionHandling();

        $formulaire = Formulaire::all()->first();
        $data['formulaireID'] = $formulaire->id;
        $data['nbr_etape'] = 0;

        $reponse = $this->post('etape/add', $data);
        $reponse->assertStatus(200);
        $this->assertDatabaseHas('etapes', ['formulaire_id'=>$formulaire->id ]);

    }

    public function test_effacer_une_etape_dans_bd(){
        $this->seed('EtapeTableSeeder');
        $etape = Etape::all()->first();
        $reponse = $this->post('etape/'.$etape->id.'/delete');
        $this->assertDatabaseMissing('etapes', ['id'=>$etape->id]);
    }


    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }
}
