<?php

namespace Tests\Unit;

use Dompdf\Exception;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use App\Formulaire;
use App\Etape;

class EtapeControllerTest extends TestCase
{
    use RefreshDatabase;
    use WithoutMiddleware;


    public function setup() : void {
        parent::setup();
        //Les formulaires doivent exister pour qu'on puisse créer une étape.
        $this->seed('FormulaireTableSeeder');
    }

    public function test_ajouter_etape_bonne_donnee() {
        $etape = Etape::all()->first();
        $formulaire = Formulaire::all()->first();

        $etape =  [
            'formulaireID' => $formulaire->id,
            'nbr_etape' => 0
        ];

        $reponse = $this->post('etape/add', $etape);
        $reponse->assertStatus(200);

        $this->assertDatabaseHas('etapes', ['formulaire_id'=>$formulaire->id] );
    }

    public function test_ajouter_avec_donnee_null() {
        $etape = Etape::all()->first();

        $etape = [];

        $reponse = $this->post('etape/add', $etape);
        $reponse->assertSessionHasErrors($key = array('formulaireID', 'nbr_etape'));
    }

    public function test_ajouter_avec_id_errone() {
        $etape = Etape::all()->first();

        $etape = [
            'formulaireID'  => 50,
            'nbr_etape'     => 0
        ];

        $reponse = $this->post('etape/add', $etape);
        $reponse->assertSessionHasErrors();
    }

    public function test_effacer_une_etape_dans_bd() {
        $this->seed('EtapeTableSeeder');
        
        $etape = Etape::all()->first();

        $reponse = $this->post('etape/'.$etape->id.'/delete');
        $this->assertDatabaseMissing('etapes', ['id'=>$etape->id]);
    }



    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }
}
