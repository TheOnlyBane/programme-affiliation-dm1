<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Formulaire extends Model
{
    public $timestamps = false;

    public function etape() {
        return $this->hasMany('App\Etape');
    }

    public function nombreQuestions(){
        $count = 0;
        foreach ($this->etape as $etape){
            $count = $count + count($etape->question);
        }
        return $count;
    }
}
