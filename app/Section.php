<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    // Attributs
    public $timestamps = false;
    protected $fillable = [
        'id',
        'information',
        'montant',
        'question_id'
    ];

    /**
     * Relation vers App\Question
     */
    public function question(){
        return $this->belongsTo('App\Question');
    }
}
