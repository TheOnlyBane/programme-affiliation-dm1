<?php

namespace App\Http\Controllers;

use App\Affilie;
use App\Projet;
use App\Http\Requests\PaiementRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Paiement;
use App\User;

class PaiementController extends Controller
{
    /**
     * Affiche la liste des paiements
     *
     * @return \Illuminate\Http\Response Vue de la liste des paiements
     */
    public function index()
    {
        $projets = Projet::all();
        $paiements = Paiement::all();
        return view('paiement.show', compact('projets', 'paiements'));
    }
    /**
     * Affiche la liste de paiement pour un USER
     * 
     * @return \Illuminate\Http\Response Vue de la liste des paiements du User
     */
    public function showUser()
    {
        $user = Auth::user();
        $projets = Projet::all()
                        ->where('affilie_id','=',$user->affilie->id);
        $paiements = collect([]);
        
        if($projets != null) {
            foreach($projets as $projet){
                $paiements->push(Paiement::where('projet_id', $projet->id)->get());
            }
        }
        $paiements = $paiements[0];
        return view('paiement.show', compact('projets', 'paiements'));
        
        
    }

    /**
     * Affiche le formulaire de création de paiements
     *
     * @return \Illuminate\Http\Response vue du formulaire de création
     */
    public function create()
    {
        $projets = Projet::all();
        $users = User::all();
        return view('paiement.create', compact('projets' , 'users'));
    }

    /**
     * Ajoute un paiement dans la base de donneés
     *
     * @param  \Illuminate\Http\Request  $request Requete HTTP
     * @return \Illuminate\Http\Response Vue de la liste es paiements
     */
    public function store(PaiementRequest $request)
    {
        $projet = Projet::find(request('projet'));
        self::ajouterCommande($projet);
        $user = User::find(request('user'));
        $paiement = new Paiement();
        self::store_paiement($request, $paiement, $projet);
        $projet->montantActuel = $projet->montantActuel - request('montant');
        $projet->save();
        return redirect('/paiement');
    }
    private function store_paiement(PaiementRequest $request, Paiement $paiement, Projet $projet){
        $paiement->type = request('type');
        $paiement->date = request('date');
        $paiement->projet_id = request('projet');
        $paiement->montant = request('montant');
        $paiement->montantProjet = $projet->montantActuel;
        $paiement->userNom = $projet->affilie->user->name;
        $paiement->userAdresse = $projet->affilie->user->adresse;
        $paiement->userApp = $projet->affilie->user->app;
        $paiement->userCodePostal = $projet->affilie->user->codePostal;
        $paiement->userProvince = $projet->affilie->user->provenance->province;
        $paiement->userTelephone = $projet->affilie->user->telephone;
        $paiement->userEmail = $projet->affilie->user->email;
        $paiement->timestamps = false;
        $paiement->save();
    }

    /**
     * Affiche le formulaire de modification de paiements
     *
     * @param  \App\Paiement  $paiement le paiement à modifier
     * @return \Illuminate\Http\Response Vue du formulaire de modification
     */
    public function edit(Paiement $paiement)
    {
        $paiement = Paiement::find($paiement->id);
        $projets = Projet::all();
        $users = User::all();
        return view('paiement.edit', compact('paiement', 'projets', 'users'));
    }

    /**
     * Modifie le paiement dans la base de données
     *
     * @param  \Illuminate\Http\Request  $request Requete HTTP
     * @param  int  $id Le ID du paiement à modifier
     * @return \Illuminate\Http\Response vue de la liste des paiements
     */
    public function update(PaiementRequest $request, $id)
    {

        $projet = Projet::find(request('projet'));
        $paiement = Paiement::findorfail($id);
        self::store_paiement($request, $paiement, $projet);
        $paiement->save();
        $paiement->projet->montantActuel = $paiement->projet->montantActuel -
                                            request('montant');

        return back()->withInput();
    }

    /**
     * Retire le paiement de la base de données
     *
     * @param  int  $id ID du paiement à retirer
     * @return \Illuminate\Http\Response Vue de la liste des paiements
     */
    public function destroy($id)
    {
        Paiement::findorfail($id)->delete();
        $paiements = Paiement::all();
        return view('paiement.show', compact('paiements'));
    }

    /**
     * Ajoute une entrée dans les statistiques
     * 
     * @param App\Projet $projet Projet qui est lié aux statistiques
     */
    private function ajouterCommande($projet){
        $statistiqueController = new StatistiqueController();
        $statistiqueController->ajouterCommande($projet->affilie->lienPartage);
    }
}
