<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Paiement;
use PDF;

class FactureController extends Controller
{
    /**
     * Crée une facture et l'affiche
     * 
     * @param int $id ID du paiement requis pour la facture
     */
    public function create($id){
        $paiement = Paiement::find($id);
        $document = PDF::loadView('facture.contenu', compact('paiement'));
        return $document->stream();
    }

    /**
     * Valide l'authentification de l'utilisateur pour chaque projet
     * 
     * @param int $isProjet ID du projet à comparer
     * @return PDF $page Le PDF à afficher
     */
    public function validateAuth($idProjet){
        $page = null;
        if (Auth::check()){
            if (Auth::user()->isAdmin == 1){
                $page = self::create($idProjet);
            }
            else{
                foreach(Auth::user()->affilie->projet as $projet){
                    if ($projet->id == $idProjet){
                        $page = self::create($idProjet);                    }
                }
            }
        }
        if ($page == null){
            abort(403, 'Action non autorisée!');
        }
    return $page;
    }

}
