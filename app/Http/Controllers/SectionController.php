<?php

namespace App\Http\Controllers;

use App\Section;
use Illuminate\Http\Request;


class SectionController extends Controller
{
    /**
     * Ajoute la section dans la base de données
     *
     * @param string $information l'information de la section
     * @param int $montant Le montant de la section
     * @param int $id l'ID de la question lié
     *
     * @return App\Section $section La section crée
     */
    public function store($id, $reponse, $montant, $question_id){
        $section = Section::create([

            'id' => $id,
            'information' => $reponse,
            'montant' => $montant,
            'question_id' => $question_id
        ]);
        $section->save();
        return $section;
    }
    /**
     * Ajoute la section dans la base de données
     *
     * @param int $id L'ID de la section
     * @param string $reponse l'information de la section
     * @param int $montant Le montant de la section
     * @param int $question_id l'ID de la question lié
     *
     * @return App\Section $section La section crée
     */
    public function update($id, $reponse, $montant, $question_id){

        if (Section::find($id)) {
            $section = Section::findorfail($id);
            $section->information = $reponse;
            $section->montant = $montant;
            $section->question_id = $question_id;
            $section->save();
            return back();
        } else {

            $this->store($id, $reponse, $montant, $question_id);
        }

    }

    /**
     * Retire la section de la base de données
     *
     * @param int $id l'ID de la section
     */
    public function destroy($id){
        $section = Section::findorfail($id);
        $section->delete();
    }
}
