<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Affilie;

class StatistiqueController extends Controller
{
    /**
     * Ajoute un entrée dans les statistique de clic
     * 
     * @param string $lien Le lien de partage de l'affilie
     * @return Illuminate\HTTP\Response La vue de l'Accueil
     */
    public function ajouterClic($lien){
        return self::ajouterStatistique($lien, "nombreClic");
    }
    /**
     * Ajoute un entrée dans les statistique de soumission
     * 
     * @param string $lien Le lien de partage de l'affilie
     * @return Illuminate\HTTP\Response La vue de l'Accueil
     */
    public function ajouterSoumission($lien){
        return self::ajouterStatistique($lien, "nombreSoumission");
    }
    /**
     * Ajoute un entrée dans les statistique de commande
     * 
     * @param string $lien Le lien de partage de l'affilie
     * @return Illuminate\HTTP\Response La vue de l'Accueil
     */
    public function ajouterCommande($lien){
        return self::ajouterStatistique($lien, "nombreCommande");
    }
    /**
     * Cherche l'utilisateur où le lien de partage correspond
     * 
     * @param string $lien Le lien de partage de l'affilié
     * @return App\Affilie  l'affilié trouvé
     */
    public function chercherUser($lien){
        $return = null;
        $affilies = Affilie::all();
        foreach($affilies as $affilie){
            if ($affilie->lienPartage == $lien){
                $return = $affilie;
            }
        }
        return $affilie;
    }
    /**
     * Ajoute la statistique sur l'affilié
     * 
     * @param string $lien Le lien de partage
     * @param string $attr Le type de statistique
     * @return Illuminate\HTTP\Response La vue de l'Accueil
     */
    private function ajouterStatistique($lien, $attr){
        $affilie = self::chercherUser($lien);
        $affilie->statistique->$attr++;
        $affilie->statistique->save();
        return view('home');
    }
}
