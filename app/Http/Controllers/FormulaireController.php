<?php

namespace App\Http\Controllers;

use App\Etape;
use App\Affilie;
use App\Formulaire;
use App\Question;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\FormulaireRequest;

class FormulaireController extends Controller
{
    /**
     * Afficher une liste de la ressource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $formulaires = Formulaire::all();
        return view('formulaire.show', compact('formulaires'));
    }

    /**
     * Montrer le formulaire pour créer une nouvelle ressource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $formulaires = Formulaire::all();
        return view('formulaire.create', compact('formulaires'));
    }

    /**
     * Stocker une ressource nouvellement créée dans le stockage.
     *
     * @param \ Illuminate \ Http \ Request $ request
     * @return \ Illuminate \ Http \ Response
     */
    public function store(FormulaireRequest $request)
    {
        $formulaire = new Formulaire();
        $formulaire->nom = $request->get('nom');
        $formulaire->isActive = false;
        $formulaire->timestamps = false;

        $formulaire->save();

        return redirect("/formulaire/$formulaire->id/edit");
    }

    /**
     * Afficher le formulaire pour modifier la ressource spécifiée.
     *
     * @param int $id id du formulaire
     * @return vu du formulaire
     */
    public function edit($id)
    {
        $formulaire = Formulaire::find($id);
        $dernier_question_id = new QuestionController();
        $dernier_id = $dernier_question_id->dernier_id();
        return view('formulaire.edit',
            compact('formulaire', 'dernier_id'));
    }

    /**
     * Mettre à jour la ressource spécifiée en stockage.
     *
     * @param \ Illuminate \ Http \ Request $ request
     * @param int $ id
     * @return \ Illuminate \ Http \ Response
     */
    public function update(FormulaireRequest $request, $id)
    {
        $formulaire = Formulaire::findorfail($id);
        $formulaire->nom = $request->get('nom');


        DB::transaction(function() use ($request) {
            $this->question($request);
            $this->etape();
        }, 5);
        

        $formulaire->save();
        return back();
    }

    /**
     * Verifie si l'etape est à supprimer
     */
    private function etape() {
        $z = 1;
        $nombre_etape = request('nbr_etape');
        while($z <= $nombre_etape) {
            if (request('etape_suprimer'.$z)) {
                $etape = new EtapeController();
                $etape->destroy(request('etape_suprimer'.$z));
            }
            $z++;
        }
    }

    /**
     * Verifie toute les question et les ajoute, modifie ou supprime
     *
     * @param $request
     */
    private function question($request) {
        $y = 1;
        $nombre_question = $request->get('nombre_question');
        while ($y <= $nombre_question)  {
            $question_id = $request->get('question-id'.$y);
            $a_supprimer = $request->get('a_supprimer'.$y);
            $question = new QuestionController();
            $condition_question = null;
            $condition_section = null;
            $type = $request->get('type'.$y);

            if ($a_supprimer) {
                $question->destroy($question_id);
            } else {
                if ($request->get('condition-verification'.$y)) {
                    $condition_question = $request->get('afficher_question'.$y);
                    $condition_section = $request->get('afficher_reponse'.$y);
                }
                if ($question_id != 'new') {
                    if (($type == 'bouton_radio') || ($type == 'checkbox')) {
                        $this->sauvegarder_choix($y, $question_id);
                    }
                    $question->update($question_id, $type,
                        $request->get('nom_question'.$y), $condition_question,
                        $condition_section);
                } else {
                    $questionid = $question->store($request->get('etape_id'.$y),
                        $request->get('type'.$y),
                        $request->get('nom_question'.$y),
                        $condition_question, $condition_section, $request);
                    if (($type == 'bouton_radio') || ($type == 'checkbox')) {
                        $this->sauvegarder_choix($y, $questionid);
                    }
                }
            }
            $y++;
        }
    }

    /**
     * Sauvegarde les choix
     *
     * @param $question_id
     */
    private function sauvegarder_choix($y, $question_id){
        $i = 1;
        $nombre_reponse = request('nbr_reponse'.$question_id);
        while ($i <= $nombre_reponse) {
            $section = new SectionController();
            $section_id = request('section-id'.$i.$question_id);
            if (request('supprimer-reponse'.$i.$question_id)) {
                $section->destroy($section_id);
            }  else {

                $section->update($section_id,
                    request('reponse_multiple'.$i.$question_id),
                    request('montant_ajouter'.$i.$question_id),
                    $question_id);
            }
            $i++;
        }
    }

    /**
     * Supprimer la ressource spécifiée de stockage.
     *
     * @param int $id
     * @return vue précédente
     */
    public function destroy($id)
    {
        Formulaire::findOrFail($id)->delete();
        return redirect()->action('FormulaireController@index');
    }

    /**
     * Affiche le formulaire pour le client
     *
     * @param $lien
     * @return vue du formulaire
     */
    public function showForm($lien){
        $formulaire = Formulaire::all()
            ->where('isActive', '=', 1)->first();
        $affilie = Affilie::where("lienPartage", '=', $lien)->get()->first();
        $affilie->statistique->incrementClic();
        $affichageEtape = [];
        $maxQuestion = 0;
        $i =1;
        foreach ($formulaire->etape as $etape) {
            $affichageEtape[$etape->id] = $i;
            foreach ($etape->question as $question) {
                if ($maxQuestion < $question->id) {
                    $maxQuestion = $question->id;
                }
            }
            $i++;
        }
        return view('formulaire.contenu',
            compact('formulaire', 'affilie',
                'affichageEtape','maxQuestion' ));
    }

    /**
     * Modifie le formulaire actif dans la base de données
     *
     * @param  Request $request La Requête HTTP
     *
     * @return \Illuminate\Http\Response vue de la liste de formulaires
     */
    public function updateActive(Request $request){
        $radio = $request->get("radioForm");
        $formulaires = Formulaire::all();
        foreach ($formulaires as $form) {
            if ($form->id == $radio){
                $form->isActive = 1;
                $form->save();
            }
            else {
                $form->isActive = 0;
                $form->save();
            }
        }
        return redirect()->back();
    }
}
