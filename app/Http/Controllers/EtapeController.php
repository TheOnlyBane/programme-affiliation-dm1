<?php

namespace App\Http\Controllers;

use App\Etape;
use App\Formulaire;
use App\Http\Requests\StoreEtape;
use Dompdf\Exception;
use Illuminate\Http\Request;

class EtapeController extends Controller
{
    /**
     * Affiche la liste des Etapes
     *
     * @return \Illuminate\Http\Response vue du formulaire
     */
    public function index()
    {
        $etapes = Etape::all();
        return view('formulaire.show', compact('etapes'));
    }

   /**
     * Montrer le formulaire pour créer une nouvelle ressource.
     *
     * @return \Illuminate\Http\Response la vue de la creation d'étape
     */
    public function create()
    {
        $etapes = Etape::all();
        return view('etape.create', compact('etapes'));
    }

    /**
     * Stocker une ressource nouvellement créée dans le stockage.
     *
     * @param \Illuminate\Http\Request $ request
     * @return \Illuminate\Http\Response la vue de la nouvelle etape
     */
    public function store(StoreEtape $request)
    {

        $etape = new Etape();
        $etape->formulaire_id = $request->get('formulaireID');

        $nbr_etape = $request['nbr_etape'] + 1;
        $formulaire = Formulaire::find($request->get('formulaireID'));
        
        if ($formulaire != null) {
            $etape->save();
            return view('etape.nouvelleEtape', compact('etape',
                'nbr_etape'));
        } else {
            return redirect()->back()
            ->withErrors(["erreur_id","Une erreur est survenue,
            vous ne pouvez pas ajouter d'étape"]);
        }
    }

    /**
     * Supprimer la ressource spécifiée de stockage.
     *
     * @param int $id l'id de l'étape à supprimer
     * @return \Illuminate\Http\Response la vue précédente
     */
    public function destroy($id)
    {
        $etape = Etape::find($id);
        $etape->delete();
        return redirect()->back();
    }
}
