<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests\AffilieUpdateRequest;
use App\Http\Requests\AffilieStoreRequest;
use App\Affilie;
use App\User;
use App\Provenance;
use App\Statistique;
use App\Note;
class AffilieController extends Controller
{

    /**
     * Affiche une liste des affilies.
     *
     * @return \Illuminate\Http\Response vue de la liste des affiliés
     */
    public function index()
    {
        $affilies = Affilie::all();
        return view('affilie.show', compact('affilies'));
    }

    /**
     * Affiche le formulaire pour la création d'un affilié
     *
     * @return \Illuminate\Http\Response vue de la création d'affiliés
     */
    public function create()
    {
        $Provenances = Provenance::all();
        return view(('affilie.create'), compact('Provenances'));
    }

    /**
     * Ajoute un affilié à la base de données
     * 
     * @param  \Illuminate\Http\Request  $request Requete HTTP
     * @return \Illuminate\Http\Response vue de la liste d'affiliés
     */
    public function store(AffilieStoreRequest $request)
    {    
        $solde = self::validateSolde($request);
        $affilie = new Affilie;
        $affilie->lienPartage = uniqid();
        $affilie->solde = 0;
        $affilie->numTPS = $request->get('numTPS');
        $affilie->numTVQ = $request->get('numTVQ');
        $affilie->nomEntreprise = $request->get('nomEntreprise');
        $affilie->numEntreprise = $request->get('numEntreprise');
        $affilie->numTelEntreprise = $request->get('numTelEntreprise');
        $affilie->commissionReccurent = $request->get('commissionReccurent');
        $affilie->commissionSimple = $request->get('commissionSimple');
        $affilie->commissionSimple = $request->get('commissionSimple');
        $affilie->save();
        $affilie->user = self::createUser($request, $affilie);
        $affilie->statistique = self::createStat($request, $affilie);
        return redirect('/affilie');
    }
    /**
     * Ajoute un utilisateur à la base de données
     * 
     * @param  \Illuminate\Http\Request  $request Requete HTTP
     * @param  \App\Affilie $affilie l'affilié où l'utilisateur créé sera lié
     * @return \App\User $user l'utilisateur retourné pour la creation
     *                                                      de l'affilie
     */
    private function createUser(AffilieStoreRequest $request, $affilie)
    {
        $user = new User;
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->password = Hash::make($request->get('password'));
        $user->telephone = $request->get('telephone');
        $user->adresse = $request->get('adresse');
        $user->codePostal = $request->get('codePostal');
        $user->provenance_id = $request->get('ProvenanceID');
        $user->affilie_id = $affilie->id;
        $user->save();
        return $user;
    }
    /**
     * Crée les statistiques de bases pour un affilié
     * 
     * @param Illuminate\Http\Request $request Requete HTTP
     * @param App\Affilie $affilie l'affilie où la statistique sera enregistrée
     * 
     * @return App\Statistique $stat les statistiques à associer à l'affilié
     */
    private function createStat(Request $request, $affilie)
    {
        $stat = new Statistique;
        $stat->nombreClic = 0;
        $stat->nombreSoumission = 0;
        $stat->nombreCommande = 0;
        $stat->affilie_id = $affilie->id;
        $stat->save();
        return $stat;
    }
    /**
     * Valide le solde d'un affilié et le met à 0 si aucun
     * 
     * @param Illuminiate\Http\Request $request Requete HTTP
     * @return int $solde le solde de l'affilié
     */
    private function validateSolde(Request $request)
    {
        if ($request->get('solde') == null || !is_numeric($request->get('solde'))){
            $solde = 0;
        } else{
            $solde = $request->get('solde');
        }
        return $solde;
    }

    /**
     * Affiche le formulaire pour modifier un affilié.
     * 
     * @param  App\Affilie $affilie l'affilié à afficher dans le formulaire
     * @return \Illuminate\Http\Response la vue du formulaire de modification
     */
    public function edit(Affilie $affilie)
    {
        $isAdmin = 0;
        $user = $affilie->user;
        $provenances = Provenance::all();
        $notes = $affilie->note;
        if (Auth::user()->isAdmin == 1){
            $isAdmin=1;
        }
        return view('affilie.edit', compact('affilie', 'user',
                                         'provenances', 'notes',
                                         'isAdmin'));
    }
    /**
     * Affiche le formulaire de modification d'affilie pour le USER
     * 
     * @return \Illuminate\Http\Response La vue du formulaire de modification
     */
    public function editUser(){
        $user = Auth::user();
        $view = $this->edit($user->affilie);
        return $view;
    }

    /**
     * Modifie l'affilié dans la base de données
     *
     * @param  \Illuminate\Http\AffilieUpdateRequest  $request Requete HTTP
     * @param  int  $id ID de l'affilié
     * @return \Illuminate\Http\Response vue de la liste des affiliés
     */
    public function update(AffilieUpdateRequest $request, $id)
    {
        $affilie = Affilie::findOrFail($id);
        $user = $affilie->user;

        $user->name = $request->get('name');
        if($request->get('password') != null){
            $user->password = Hash::make($request->get('password'));
        }
        $user->telephone = $request->get('telephone');
        $user->adresse = $request->get('adresse');
        $user->codePostal = $request->get('codePostal');
        $user->app = $request->get('app');
        $user->provenance_id = $request->get('ProvenanceID');
        $user->save();

        $affilie->solde = $request->get('solde');
        $affilie->commissionReccurent = $request->get('commissionReccurent');
        $affilie->commissionSimple = $request->get('commissionSimple');
        $affilie->numTPS = $request->get('numTPS');
        $affilie->numTVQ = $request->get('numTVQ');
        $affilie->nomEntreprise = $request->get('nomEntreprise');
        $affilie->numEntreprise = $request->get('numEntreprise');
        $affilie->numTelEntreprise = $request->get('numTelEntreprise');
        $affilie->save();

        return redirect('/affilie');
    }
    /**
     * Modifie l'affilié (user) dans la base de données
     * 
     * @param  \Illuminate\Http\Request  $request Requete HTTP
     * @param  int  $id ID de l'affilié
     */
    public function updateUser(AffilieUpdateRequest $request, $id){
        self::update($request, $id);
        return back();
    }

    /**
     * Retire l'affilié de la base de donneés
     *
     * @param  int  $id ID de l'affilié
     * @return \Illuminate\Http\Response vue de la liste des affiliés
     */
    public function destroy(Affilie $affilie)
    {
        
        if ($affilie != null){
            $user = $affilie->user;
            $stat = $affilie->statistique;
            foreach ($affilie->projet as $projet) {
                foreach ($projet->paiement as $paiement) {
                    $paiement->delete();
                }
                $projet->delete();
            }
            $stat->delete();
            $user->delete();
            $affilie->delete();

        }
        
        return redirect('/affilie');
    }
}
