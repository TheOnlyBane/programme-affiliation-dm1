<?php

namespace App\Http\Controllers;

use App\Etape;
use App\Formulaire;
use App\Question;
use App\Section;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class QuestionController extends Controller
{
    /**
     * Stocker une ressource nouvellement créée dans le stockage.
     *
     * @param $etape_id
     * @param $type
     * @param $nom_question
     * @param $condition_question
     * @param $condition_section
     * @param $request
     * @return question->id id de la nouvelle question
     */
    public function store($etape_id, $type, $nom_question, $condition_question,
                          $condition_section, $request)
    {
        $question = Question::create([
            'nom' => $nom_question,
            'type'  => $type,
            'etape_id'  => $etape_id,
            'conditionParent' => $condition_question,
            'conditionSection' => $condition_section
        ]);

        $question->save();

        return $question->id;
    }

    /**
     * Mettre à jour la ressource spécifiée en stockage.
     *
     * @param etape_id id de l'etape actuel
     * @param type le type de la question
     * @param nom_question le nom de la question
     * @param condition_question la condition parent de la question
     * @param condition_section la condition enfant de la question
     */
    public function update($id, $type, $nom_question, $condition_question,
                           $condition_section)
    {
        $question = Question::findorfail($id);
        $question->type = $type;
        $question->nom = $nom_question;
        $question->conditionParent = $condition_question;
        $question->conditionSection = $condition_section;
        $question->save();
    }

    /**
     * Supprimer la ressource spécifiée de stockage.
     *
     * @param int $id id de la question à supprimer
     * @return la vue précédente
     */
    public function destroy($id)
    {
        $question = Question::find($id);
        $question->delete();
        return redirect()->back();
    }

    /**
     * Permet l'affichage des questions
     *
     * @return la vue d'afficher question
     */
    public function afficher_question(){
        $data = request()->all();
        $type_question = $data['type_question'];
        $question_id = $data['question_id'];
        $nbr_question = $data['nbr_question'];
        $etape_id = $data['etape_id'];
        $section_id = $data['section_id'];
        $questions = Question::all();
        $dernier_id = $data['dernier_id'];

        return view('question.afficher_question',
            compact('type_question', 'question_id', 'nbr_question',
                'etape_id', 'questions', 'dernier_id', 'section_id'));

    }

    /**
     * Permet l'affichage des sections dans la question
     *
     * @return la vue d'afficher section
     */
    public function afficher_section(){
        $data = request()->all();
        $question_id = $data['question_id'];
        $question = Question::findorfail($question_id);

        return view('question.afficher_section',
            compact('question_id', 'question'));
    }

    /**
     * Permet de chercher le dernier id incrementé
     *
     * @return le dernier id de question
     */
    public function dernier_id(){
        $statement = DB::select("show table status like 'questions'");
        return $statement[0]->Auto_increment;
    }

}
