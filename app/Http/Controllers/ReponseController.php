<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\ReponseRequest;
use App\Projet;
use App\Reponse;
use App\Question;
use App\Section;
use App\Formulaire;

class ReponseController extends Controller
{
    /**
     * Affiche le resultat de toutes les réponses
     * 
     * @param App\Projet $projet le Projet où les réponses sont associées
     * @return Illuminate\HTTP\Response Vue du résultat du formulaire
     */
    public function show($id){
        $projet = Projet::findorfail($id);
        return view('formulaire.resultat', compact('projet'));
    }
    /**
     * Ajoute une réponse dans la base de données
     * 
     * @param Illuminate\HTTP\Request $request Requete HTTP
     * @return Illuminate\HTTP\Response Vue du résultat final
     */
    public function store(Request $request){
        $projets = Projet::all();
        $count = count($projets)+1;
        $projet = Projet::create([
            'type'  => 'Simple',
            'nom'  => 'EST-'.$count,
            'status' => 'En Attente',
            'date'  => date('Y-m-d'),
            'commissionPourcentage' => 0,
            'montantTotal' => 0,
            'montantActuel'=> 0,
            'affilie_id' => $request->get('affilie')
        ]);
        $projet->affilie->statistique->incrementSoumission();
        DB::transaction(function () use($request, $projet) {
            self::createReponse($request, $projet);
        }, 5);

        return view('formulaire.resultat', compact('projet'));
    }

    /**
     * Vérifie la méthode de sauvegarde de réponses
     * 
     * @param Illuminate\HTTP\Request $request Requete HTTP
     * @param App\Projet $projet le projet associé
     */
    private function createReponse(Request $request, Projet $projet) {
        $formulaire = Formulaire::all()
                ->where('id','=',$request->get('formulaire'))->first();
        $montantFinal = 0;
        foreach($formulaire->etape as $etape){
            foreach($etape->question as $question){
                if ($question->type == "checkbox" ||
                    $question->type == "bouton_radio"){
                    foreach ($question->section as $section){
                        if ($question->type == "checkbox"){
                            $montantFinal = $montantFinal + 
                                    self::manage_checkbox(
                                        $request, $question, $section, $projet
                                    );
                        }
                        else if ($question->type == "bouton_radio") {
                            $montantFinal = $montantFinal + 
                                    self::manage_radio(
                                        $request, $question, $section, $projet
                                    );
                        }
                    }
                }
                else {
                    self::manage_input($request, $question, $projet);
                }
            }
        }
        $projet->montantTotal = $montantFinal;
        $projet->save();
    }
    /**
     * Vérifie l'existence de la section et la sauvegarde dans le checkbox
     * 
     * @param Illuminate\HTTP\Request $request Requete HTTP
     * @param App\Question $question la question à vérifier
     * @param App\Section $section la section de la question à sauvegarder
     * @param App\Projet $projet le projet associé
     * 
     * @return int $montantFinal Le montant final de la soumission
     */
    private function manage_checkbox($request, $question, $section, $projet){
        $montantFinal = 0;
        if ($request->get('question'.$question->id.'-'.$section->id) != null) {
            if (self::parentEnable($question, $request)){
                self::saveReponse($question, $projet, $section->information);
                $montantFinal = $montantFinal + $section->montant;
            }
        }
        return $montantFinal;
    }
     /**
     * Vérifie l'existence de la section et la sauvegarde dans le bouton radio
     * 
     * @param Illuminate\HTTP\Request $request Requete HTTP
     * @param App\Question $question la question à vérifier
     * @param App\Section $section la section de la question à sauvegarder
     * @param App\Projet $projet le projet associé
     * 
     * @return int $montantFinal Le montant final de la soumission
     */
    private function manage_radio($request, $question, $section, $projet) {
        $montantFinal = 0;
        if (self::parentEnable($question, $request)){
            if ($request->get('question'.$question->id) != null &&
                $request->input('question'.$question->id) ==
                $section->information) {
                self::saveReponse(
                    $question, $projet, $request->get('question'.$question->id)
                );
                $montantFinal = $montantFinal + $section->montant;

            }
        }
        return $montantFinal;
    }
     /**
     * Vérifie l'existence de la section et la sauvegarde des entrées de texte
     * 
     * @param Illuminate\HTTP\Request $request Requete HTTP
     * @param App\Question $question la question à vérifier
     * @param App\Projet $projet le projet associé
     * 
     * @return int $montantFinal Le montant final de la soumission
     */
    private function manage_input($request, $question, $projet){
        $montantFinal = 0;
        if($request->get('question'.$question->id) != null){
            if (self::parentEnable($question, $request)){
                self::saveReponse(
                    $question, $projet, $request->get('question'.$question->id)
                );

            }
        }
        return $montantFinal;
    }

    /**
     * Vérifie si le parent de la question est activé
     * 
     * @param App\Question $question La Question à vérifier
     * @param Illuminate\HTTP\Request $request Requete HTTP
     * @return boolean $retour Retourne vrai si le parent est activé
     */
    private function parentEnable(Question $question, Request $request){
        $retour = true;
        $parent = null;
        if ($question->conditionParent != null){
            $section = Section::findorfail($question->conditionSection);
            if ($request->get(
                'question'.$question->conditionParent.
                '-'.$question->conditionSection) != null) {
                $parent = Question::findorfail($question->conditionParent);
            }
            if ($request->get(
                'question'.$question->conditionParent) != null &&
                $request->input(
                'question'.$question->conditionParent) ==
                $section->information){
                $parent = Question::findorfail($question->conditionParent);
            }
            if ($parent != null){
                $retour = self::parentEnable($parent, $request);
            }
            else{
                $retour = false;
            }
        }
        return $retour;
    }

    /**
     * Sauvegarde la réponse dans la base de données
     * 
     * @param App\Question $question La question où le nom est utilisé
     * @param App\Projet $projet Projet lié à la réponse
     * @param String $information le contenu de la réponse
     */
    private function saveReponse( $question, $projet, $information) {
        $reponse = Reponse::create([
            "question" => $question->nom,
            "reponse" => $information,
            "projet_id" => $projet->id
        ]);
        $reponse->save(); 
    }
}



?>