<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\NoteRequest;
use App\Affilie;
use App\User;
use App\Note;
use App\Provenance;

class NoteController extends Controller
{


    /**
     * Ajoute une note dans la base de données par AJAX
     *
     * @param  \Illuminate\Http\Request  $request Requete HTTP
     * @return \Illuminate\Http\Response vue de la nouvelle note
     */
    public function store(NoteRequest $request)
    {
        $data = request()->all();
        $uneNote = Note::create([
            'description' => $data['description'],
            'date' => date('Y-m-d'),
            'affilie_id' => $data['affilieid']
        ]);
        return view('note.nouvelleNote', compact('uneNote'));
    }

    /**
     * Modifie la note dans la base de données par AJAX
     *
     * @param  \Illuminate\Http\Request  $request Requete HTTP
     * @param  int  $id ID de la note à modifier
     * @return \Illuminate\Http\Response JSON pour le AJAX
     */
    public function update(NoteRequest $request, $noteID)
    {
        $request = request()->all();
        $note = Note::find($noteID);
        $note->description = $request->get('description');
        $note->date = $request->get('date');
        $note->save();
        return response()->json(['success'=>$request['description']]);
    }

    /**
     * Retire la note de la base de données
     *
     * @param  int  $id le ID de la note à supprimer
     * @return \Illuminate\Http\Response JSON pour le AJAX
     */
    public function destroy($noteID)
    {
        $note = Note::find($noteID);
        $note->delete();
        return response()->json(['success'=>'success']);
    }
}
