<?php

namespace App\Http\Controllers;

use App\Affilie;
use Illuminate\Http\Request;
use App\Projet;
use App\User;
use App\Http\Requests\ProjetStoreRequest;

class ProjetController extends Controller
{
    /**
     * Affiche la liste des projets
     *
     * @return \Illuminate\Http\Response Vue de la liste des projets
     */
    public function index()
    {
        $projets = Projet::all();
        return view('projet.show', compact('projets'));
    }

    /**
     * Affiche le formulaire de création de projets
     *
     * @return \Illuminate\Http\Response Vue du formulaire de création
     */
    public function create()
    {
        $affilies = Affilie::all();
        return view('projet.create', compact('affilies'));
    }

    /**
     * Ajoute le projet dans le base de données
     *
     * @param  \Illuminate\Http\Request  $request Requete HTTP
     * @return \Illuminate\Http\Response Vue de la liste de projets
     */
    public function store(ProjetStoreRequest $request)
    {
        
        $projet = Projet::create([
            'type'  => $request->get('type'),
            'nom'  => $request->get('nom'),
            'status' => $request->get('status'),
            'date'  => $request->get('date'),
            'commissionPourcentage' => $request->get('commission'),
            'montantTotal' => $request->get('montantTotal'),
            'montantActuel'=> $request->get('montantTotal'),
            'affilie_id' => $request->get('affilie')
        ]);
        $projet->save();
        
        return redirect()->action('ProjetController@index');
    }

    /**
     * Affiche le formulaire de modification de projets
     *
     * @param  int  $id ID du projet à modifier
     * @return \Illuminate\Http\Response Vue du formulaire de modification
     */
    public function edit(Projet $projet)
    {
        $affilies = Affilie::all();
        return view('projet.edit', compact('projet', 'affilies'));
    }

    /**
     * Modifie le projet dans la base de données
     *
     * @param  \Illuminate\Http\Request  $request Requete HTTP
     * @param  int  $id ID du projet à modifier
     * @return \Illuminate\Http\Response Vue du formulaire de modification
     */
    public function update(ProjetStoreRequest $request, $id)
    {
        $projet = Projet::findOrFail($id);
        
        self::updateStats($projet, $request->get('status'));
        $projet->type = $request->get('type');
        $projet->timestamps = false;
        $projet->status = $request->get('status');
        $projet->nom = $request->get('nom');
        $projet->commissionPourcentage = $request->get('commission');
        $projet->montantTotal = $request->get('montantTotal');
        $projet->affilie_id = $request->get('affilie');
        $projet->date = $request->get('date');
        
        $projet->save();
        return redirect()->back();
    }

    /**
     * Retire le projet de la base de données
     *
     * @param  int  $id ID du projet
     * @return \Illuminate\Http\Response Vue de la liste des projets
     */
    public function destroy($id)
    {
        Projet::findOrFail($id)->delete();
        return redirect('/projet');
    }

    private function updateStats($projet, $request){
        if ($projet->status == "en_attente" && $request == "en_cours"){
            $projet->affilie->statistique->incrementCommande();
        }

    }

    
}
