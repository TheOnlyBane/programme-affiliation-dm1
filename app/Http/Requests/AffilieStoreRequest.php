<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AffilieStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'min:3', 'max:50'],
            'telephone' => ['required', 'min:10', 'max:25'],
            'adresse'=>['required', 'max:100'],
            'codePostal'=>['required', 'max:50'],
            'app' => ['max:50'],
            'ProvenanceID'=> ['required'],
            'nomEntreprise'=>['required', 'min:3', 'max:50'],
            'numEntreprise'=>['required', 'min:3', 'max:50'],
            'numTelEntreprise'=> ['required','min:10', 'max:50'],
            'commissionReccurent' => ['required'],
            'commissionSimple' => ['required'],
            'numTPS'=> ['required', 'max:50'],
            'numTVQ'=> ['required', 'max:50'],
            'email' => ['required', 'email', 'unique:users'],
            'password' => ['required', 'min:8'],
            'solde' => ['integer', 'min:0']
        ];
    }
}
