<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PaiementRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => ["required", "max:25"],
            'montant' => "required",
            'userNom' => "max:50",
            'userAdresse' => "max:50",
            'userApp' => "max:50",
            'userCodePostal' => "max:50",
            'userProvince' => "max:50",
            'userTelephone' => "max:50",
            'userEmail' => "max:50",
            'date' => 'required',
        ];
    }
}
