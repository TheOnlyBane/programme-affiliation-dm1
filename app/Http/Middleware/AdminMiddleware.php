<?php

namespace App\Http\Middleware;

use Closure;
class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    protected $auth;
    public function handle($request, Closure $next)
    {
        if ($request->user())
        {
            if ($request->user()->isAdmin != 1){
                abort(403, 'Action non authorisée');
            }
        }
        else {
            abort(403, 'Veuillez vous connecter !');
        }
        return $next($request);
    }
}
