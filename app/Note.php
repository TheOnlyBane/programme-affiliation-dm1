<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    // Attributs
    public $timestamps = false;
    protected $fillable = [
        'description',
        'date',
        'affilie_id'
    ];

    /**
     * Relation vers App\Affilie
     */
    public function affilie(){
        return $this->belongsTo('App\Affilie');
    }
}
