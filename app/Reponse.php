<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reponse extends Model
{
    // Attributs
    public $timestamps = false;
    protected $fillable = [
        'question',
        'reponse',
        'projet_id'
    ];
}
