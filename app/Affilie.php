<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Affilie extends Model
{
    // Attributs
    public $timestamps = false;
    protected $fillable = [
        'lienPartage',
        'solde',
        'commReccurent',
        'commSimple',
        'numTPS',
        'numTVQ',
        'nomEntreprise',
        'numEntreprise',
        'numTelEntreprise',
        'statistique_id'
    ];

    /**
     * Relation vers App\Projet
     */
    public function projet(){
        return $this->hasMany('App\Projet');
    }
    /**
     * Relation vers App\User
     */
    public function user(){
        return $this->hasOne('App\User');
    }
    /**
     * Relation vers App\Statistique
     */
    public function statistique(){
        return $this->hasOne('App\Statistique');
    }
    /**
    * Relation vers App\Note
    */
    public function note(){
        return $this->hasMany('App\Note');
    }
}
