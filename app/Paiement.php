<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paiement extends Model
{
    public $timestamps = false;

    /**
     * Relation vers App\Projet
     */
    public function projet(){
        return $this->belongsTo('App\Projet');
    }
}
