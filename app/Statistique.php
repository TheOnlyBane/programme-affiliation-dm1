<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Statistique extends Model
{
    // Attributs
    public $timestamps = false;
    protected $fillable = [
        'nombreClic',
        'nombreSoumission',
        'nombreCommande',
        'affilie_id'
    ];

    /**
     * Relation vers App\Affilie
     */
    public function affilie(){
        return $this->belongsTo('App\Affilie');
    }

    /**
     * Incrémente la statistique Clic
     */
    public function incrementClic(){
        $this->nombreClic++;
        $this->save();
    }

    /**
     * Incrémente la statistique Soumission
     */
    public function incrementSoumission(){
        $this->nombreSoumission++;
        $this->save();
    }

    /**
     * Incrémente la statistique Commande
     */
    public function incrementCommande(){
        $this->nombreCommande++;
        $this->save();
    }
}
