<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    //Atributs
    public $timestamps = false;
    private $__reponses;
    private $__nom;
    private $__conditions;
    private $extracted = false;

    protected $fillable = [
        'type',
        'nom',
        'etape_id',
        'condition',
        'conditionParent',
        'conditionSection',
    ];

    /**
     * Relation vers App\Etape
     */
    public function etape() {
        return $this->belongsTo('App\Etape');
    }

    /**
     * Relation vers App\Section
     */
    public function section(){
        return $this->hasMany('App\Section');
    }
}
