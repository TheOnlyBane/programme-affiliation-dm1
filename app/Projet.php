<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Projet extends Model
{
    //Attributs
    public $timestamps = false;
    
    protected $fillable = [
        'type',
        'nom',
        'status',
        'date',
        'commissionPourcentage',
        'montantTotal',
        'montantActuel',
        'affilie_id',

    ];

    /**
     * Relation vers App\Affilie
     */
    public function affilie(){
        return $this->belongsTo('App\Affilie');
    }

    /**
     * Relation vers App\Paiement
     */
    public function paiement(){
        return $this->hasMany('App\Paiement');
    }
}
