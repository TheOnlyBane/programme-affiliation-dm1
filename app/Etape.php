<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Etape extends Model
{
    // Attributs
    public $timestamps = false;
    protected $fillable = [
        'type',
        'status',
        'date',
        'commissionPourcentage',
        'affilie_id',

    ];

    /**
    * Relation vers App\Formulaire
    */
    public function formulaire() {
        return $this->belongsTo('App\Formulaire');
    }

    /**
    * Relation vers App\Question
    */
    public function question() {
        return $this->hasMany('App\Question');
    }
}
