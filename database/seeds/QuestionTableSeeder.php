<?php

use Illuminate\Database\Seeder;

class QuestionTableSeeder extends Seeder
{
    /**
     * Ajoute les entrées dans la base de données
     *
     * @return void
     */
    public function run()
    {
        DB::table('questions')->insert([
            'nom'  => "Nom",
            'type'  => 'input',
            'conditionParent'=> null,
            'conditionSection' => null,
            'etape_id' => '1',
        ]);
        DB::table('questions')->insert([
            'nom'  => "Commentaires",
            'type'  => 'zone_texte',
            'conditionParent' => null,
            'conditionSection' => null,
            'etape_id' => '1',
        ]);
        DB::table('questions')->insert([
            'nom'  => "Options",
            'type'  => 'checkbox',
            'conditionParent' => null,
            'conditionSection' => null,
            'etape_id' => '1',
        ]);
        DB::table('questions')->insert([
            'nom'  => "Type de site web",
            'type'  => 'bouton_radio',
            'conditionParent' => 3,
            'conditionSection' => 2,
            'etape_id' => '1',
        ]);
        DB::table('questions')->insert([
            'nom'  => "Nom du site Statique",
            'type'  => 'input',
            'conditionParent' => 4,
            'conditionSection' => 5,
            'etape_id' => '1',
        ]);
        DB::table('questions')->insert([
            'nom'  => "Nom du site Dynamique",
            'type'  => 'input',
            'conditionParent' => 4,
            'conditionSection' => 6,
            'etape_id' => '1',
        ]);
        DB::table('questions')->insert([
            'nom'  => "Nom du site Mixe",
            'type'  => 'input',
            'conditionParent' => 4,
            'conditionSection' => 7,
            'etape_id' => '1',
        ]);
        DB::table('questions')->insert([
            'nom'  => "Combien de produits",
            'type'  => 'checkbox',
            'conditionParent' => null,
            'conditionSection' => null,
            'etape_id' => '2',
        ]);
    }
}
