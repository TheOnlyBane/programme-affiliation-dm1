<?php

use Illuminate\Database\Seeder;

class SectionTableSeeder extends Seeder
{
    /**
     * Ajoute les entrées dans la base de données
     *
     * @return void
     */
    public function run()
    {
        DB::table('sections')->insert([
            'id' => 1,
            'information' => "Blog",
            'montant' => 150,
            'question_id' => 3
        ]);
        DB::table('sections')->insert([
            'id' => 2,
            'information' => "Commerce",
            'montant' => 250,
            'question_id' => 3
        ]);
        DB::table('sections')->insert([
            'id' => 3,
            'information' => "Information",
            'montant' => 200,
            'question_id' => 3
        ]);
        DB::table('sections')->insert([
            'id' => 4,
            'information' => "Activite",
            'montant' => 100,
            'question_id' => 3
        ]);
        DB::table('sections')->insert([
            'id' => 5,
            'information' => "Statique",
            'montant' => 150,
            'question_id' => 4
        ]);
        DB::table('sections')->insert([
            'id' => 6,
            'information' => "Dynamique",
            'montant' => 350,
            'question_id' => 4
        ]);
        DB::table('sections')->insert([
            'id' => 7,
            'information' => "Mixe",
            'montant' => 400,
            'question_id' => 4
        ]);
        DB::table('sections')->insert([
            'id' => 8,
            'information' => "1-25",
            'montant' => 100,
            'question_id' => 8
        ]);
        DB::table('sections')->insert([
            'id' => 9,
            'information' => "26-50",
            'montant' => 200,
            'question_id' => 8
        ]);
    }
}
