<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Lance tous les seeders de la base de données
     *
     * @return void
     */
    public function run()
    {
        $this->call(AffilieTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(ProjetTableSeeder::class);
        $this->call(PaiementTableSeeder::class);
        $this->call(FormulaireTableSeeder::class);
        $this->call(EtapeTableSeeder::class);
        $this->call(QuestionTableSeeder::class);
        $this->call(SectionTableSeeder::class);
    }
}
