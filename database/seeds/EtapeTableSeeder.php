<?php

use Illuminate\Database\Seeder;
use App\Formulaire;
use App\Etape;


class EtapeTableSeeder extends Seeder
{
    /**
     * Ajoute les entrées dans la base de données
     *
     * @return void
     */
    public function run()
    {

        $formulaire1 = Formulaire::all()->first();
        $formulaire2 = Formulaire::all()->last();

        $etape1 = new Etape();
        $etape1->formulaire_id = $formulaire1->id;
        $etape1->save();

        $etape2 = new Etape();
        $etape2->formulaire_id = $formulaire1->id;
        $etape2->save();

        $etape4 = new Etape();
        $etape4->formulaire_id = $formulaire1->id;
        $etape4->save();

        $etape3 = new Etape();
        $etape3->formulaire_id = $formulaire2->id;
        $etape3->save();
    }
}
