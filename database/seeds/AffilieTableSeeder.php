<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Affilie;
use App\Statistique;

class AffilieTableSeeder extends Seeder
{
    /**
     * Ajoute les entrées dans la base de données
     *
     * @return void
     */
    public function run()
    {

        $affilie = new Affilie();
        $affilie->lienPartage = uniqid();
        $affilie->solde = 1000;
        $affilie->commissionSimple = 15;
        $affilie->commissionReccurent = 10;
        $affilie->numTPS = '1234-5678';
        $affilie->numTVQ = '1234-5678';
        $affilie->nomEntreprise = 'Elefen';
        $affilie->numEntreprise = '1234-5678';
        $affilie->numTelEntreprise = '1234567890';
        $affilie->save();

        $affilie1 = new Affilie();
        $affilie1->lienPartage = uniqid();
        $affilie1->solde = 1000;
        $affilie1->commissionSimple = 15;
        $affilie1->commissionReccurent = 10;
        $affilie1->numTPS = '1234-5678';
        $affilie1->numTVQ = '1234-5678';
        $affilie1->nomEntreprise = 'Elefen';
        $affilie1->numEntreprise = '1234-5678';
        $affilie1->numTelEntreprise = '1234567890';
        $affilie1->save();

        $stat = new Statistique();
        $stat->nombreClic = 100;
        $stat->nombreSoumission = 100;
        $stat->nombreCommande = 100;
        $stat->affilie()->associate($affilie);
        $stat->save();

        $stat = new Statistique();
        $stat->nombreClic = 100;
        $stat->nombreSoumission = 100;
        $stat->nombreCommande = 100;
        $stat->affilie()->associate($affilie1);
        $stat->save();
    }
}
