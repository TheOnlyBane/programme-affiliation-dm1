<?php

use Illuminate\Database\Seeder;
use App\Projet;
use App\User;
use App\Paiement;

class PaiementTableSeeder extends Seeder
{
    /**
     * Ajoute les entrées dans la base de données
     *
     * @return void
     */
    public function run()
    {
        
        $projet1 = Projet::all()->first();
        $projet2 = Projet::all()->last();
        $user1 = User::all()->first();
        $user2 = User::all()->last();


        $paiements1 = new Paiement();
        $paiements1->type = 'recurrent';
        $paiements1->date = '2019-10-02';
        $paiements1->montant = '2000';
        $paiements1->montantProjet = $projet1['montantActuel'];
        $paiements1->userNom = $user1->name;
        $paiements1->userAdresse = $user1->adresse;
        $paiements1->userApp = $user1->app;
        $paiements1->userCodePostal = $user1->codePostal;
        $paiements1->userProvince = $user1->provenance->province;
        $paiements1->userTelephone = $user1->telephone;
        $paiements1->userEmail = $user1->email;
        $paiements1->projet_id = $projet1['id'];

        $paiements2 = new Paiement();
        $paiements2->type = 'unique';
        $paiements2->date = '2019-10-08';
        $paiements2->montant = '5000';
        $paiements2->montantProjet = $projet2->montantActuel;
        $paiements2->userNom = $user2->name;
        $paiements2->userAdresse = $user2->adresse;
        $paiements2->userApp = $user2->app;
        $paiements2->userCodePostal = $user2->codePostal;
        $paiements2->userProvince = $user2->provenance->province;
        $paiements2->userTelephone = $user2->telephone;
        $paiements2->userEmail = $user2->email;
        $paiements2->projet_id = $projet2->id;

        $projet1->montantActuel = $projet1->montantActuel - 2000;
        $projet2->montantActuel = $projet2->montantActuel - 5000;
        $projet1->save();
        $projet2->save();
        
        $paiements1->save();
        $paiements2->save();

    }
}
