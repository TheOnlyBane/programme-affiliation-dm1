<?php

use Illuminate\Database\Seeder;
use App\Projet;
use App\Affilie;

class ProjetTableSeeder extends Seeder
{
    /**
     * Ajoute les entrées dans la base de données
     *
     * @return void
     */
    public function run()
    {

        $affilie1 = Affilie::all()->first();
        $affilie2 = Affilie::all()->last();

        $projet1 = new Projet();
        $projet1->type = 'recurrent';
        $projet1->nom = 'EST-001';
        $projet1->date = '2019-10-02';
        $projet1->status = 'en_attente';
        $projet1->commissionPourcentage = 10;
        $projet1->montantTotal = 10000;
        $projet1->montantActuel = 10000;
        $projet1->affilie_id = $affilie1->id;

        $projet2 = new Projet();
        $projet2->type = 'recurrent';
        $projet2->nom = 'EST-002';
        $projet2->date = '2019-10-04';
        $projet2->status = 'en_attente';
        $projet2->commissionPourcentage = 15;
        $projet2->montantTotal = 15000;
        $projet2->montantActuel = 15000;
        $projet2->affilie_id = $affilie2->id;

        $projet1->save();
        $projet2->save();
    }
}
