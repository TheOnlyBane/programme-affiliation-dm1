<?php

use Illuminate\Database\Seeder;
use App\Formulaire;

class FormulaireTableSeeder extends Seeder
{
    /**
     * Ajoute les entrées dans la base de données
     *
     * @return void
     */
    public function run()
    {
        $formulaire1 = new Formulaire();
        $formulaire1->nom = 'Formulaire FR';
        $formulaire1->isActive = true;
        $formulaire1->save();

        $formulaire2 = new Formulaire();
        $formulaire2->nom = 'Formulaire EN';
        $formulaire2->isActive = false;
        $formulaire2->save();
    }
}
