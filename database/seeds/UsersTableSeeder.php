<?php

use Illuminate\Database\Seeder;
use App\Affilie;
use App\Provenance;
use App\User;
class UsersTableSeeder extends Seeder
{
    /**
     * Ajoute les entrées dans la base de données
     *
     * @return void
     */
    public function run()
    {
        $affilies = Affilie::all();
        $provenance = new Provenance();
        $provenance->pays= "Canada";
        $provenance->province = "Quebec";
        $provenance->save();

        $user = new User();
        $user->name="Admin";
        $user->password=Hash::make('banane123');
        $user->email="admin@admin.com";
        $user->telephone = "81981848422";
        $user->adresse = "une adresse";
        $user->codePostal = "J2B XXX";
        $user->isAdmin = 1;
        $user->provenance()->associate($provenance);
        $user->affilie()->associate($affilies[0]);
        $user->save();

        $user = new User();
        $user->name="User";
        $user->password=Hash::make('banane123');
        $user->email="user@user.com";
        $user->telephone = "81981848422";
        $user->adresse = "une adresse";
        $user->codePostal = "J2B XXX";
        $user->isAdmin = 0;
        $user->provenance()->associate($provenance);
        $user->affilie()->associate($affilies[1]);
        $user->save();
      
        
    }
}
