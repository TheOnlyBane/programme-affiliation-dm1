<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Statistique extends Migration
{
    /**
     * Lance la migration
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Statistiques', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('nombreClic');
            $table->integer('nombreSoumission');
            $table->integer('nombreCommande');
            $table->bigInteger('affilie_id')->unsigned()->unique();
            $table->foreign('affilie_id')->references('id')->on('Affilies');
        });
    }

    /**
     * Retire la migration
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Statistiques');
    }
}
