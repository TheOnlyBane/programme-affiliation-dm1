<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Section extends Migration
{
    /**
     * Lance la migration
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Sections', function (Blueprint $table) {
            $table->bigInteger('id');
            $table->string('information', 70);
            $table->integer('montant');
            $table->bigInteger('question_id')->unsigned();
            $table->foreign('question_id')->references('id')->on('Questions')
                    ->onDelete('cascade');
        });
    }

    /**
     * Retire la migration
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Sections');
    }
}
