<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Question extends Migration
{
    /**
     * Lance la migration
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Questions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type', 25);
            $table->string('nom', 500);
            $table->integer('conditionParent')->nullable();
            $table->integer('conditionSection')->nullable();
            $table->bigInteger('etape_id')->unsigned();
            $table->foreign('etape_id')->references('id')->on(
                'Etapes')->onDelete('cascade');
        });
    }

    /**
     * Retire la migration
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Questions');
    }
}
