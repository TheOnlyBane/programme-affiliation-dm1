<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Lance la migration
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 50);
            $table->string('email', 50)->unique();
            $table->string('telephone', 25);
            $table->string('adresse', 100);
            $table->string('app', 50)->nullable();
            $table->string('password', 100);
            $table->integer('isAdmin')->default(0);
            $table->string('codePostal', 50);
            $table->bigInteger('provenance_id')->unsigned();
            $table->bigInteger('affilie_id')->unsigned();
            $table->foreign('affilie_id')->references('id')->on('Affilies');
            $table->foreign('provenance_id')->references('id')->on(
                'Provenances');
            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Retire la migration
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
