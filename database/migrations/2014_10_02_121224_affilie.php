<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Affilie extends Migration
{
    /**
     * Lance la migration
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Affilies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('lienPartage', 100);
            $table->integer('solde')->default(0);
            $table->integer('commissionReccurent')->default(0);
            $table->integer('commissionSimple')->default(0);
            $table->string('numTPS', 50)->nullable();
            $table->string('numTVQ', 50)->nullable();
            $table->string('nomEntreprise', 50);
            $table->string('numEntreprise', 50)->nullable();
            $table->string('numTelEntreprise', 50);
            
        });
    }

    /**
     * Retire la migration
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Affilies');
    }
}
