<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Paiement extends Migration
{
    /**
     * Lance la migration
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Paiements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type', 25);
            $table->integer('montant');
            $table->integer('montantProjet');
            $table->string('userNom', 50);
            $table->string('userAdresse', 50);
            $table->string('userApp', 50)->nullable();
            $table->string('userCodePostal', 50);
            $table->string('userProvince', 50);
            $table->string('userTelephone', 50);
            $table->string('userEmail', 50);
            $table->date('date');
            $table->bigInteger('projet_id')->unsigned();
            $table->foreign('projet_id')->references('id')->on('Projets');
        });
    }

    /**
     * Retire la migration
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Paiements');
    }
}
