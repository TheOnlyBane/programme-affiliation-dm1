<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Reponse extends Migration
{
    /**
     * Lance la migration
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Reponses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('question', 100);
            $table->string('reponse', 100);
            $table->bigInteger('projet_id')->unsigned();
            $table->foreign('projet_id')->references('id')->on('Projets');
        });
    }

    /**
     * Retire la migration
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Reponses');
    }
}
