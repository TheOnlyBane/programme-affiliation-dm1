<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Projet extends Migration
{
    /**
     * Lance la migration
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Projets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type', 50);
            $table->string('nom', 50);
            $table->string('status', 25);
            $table->date('date');
            $table->integer('commissionPourcentage');
            $table->integer('montantTotal');
            $table->integer('montantActuel');
            $table->bigInteger('affilie_id')->unsigned();
            $table->foreign('affilie_id')->references('id')->on('Affilies');
        });
    }

    /**
     * Retire la migration
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Projets');
    }
}
