<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Etape extends Migration
{
    /**
     * Lance la migration
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Etapes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('formulaire_id')->unsigned();
            $table->foreign('formulaire_id')->references('id')
                ->on('Formulaires')
                ->onDelete('cascade');
        });
    }

    /**
     * Retire la migration
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Etapes');
    }
}
