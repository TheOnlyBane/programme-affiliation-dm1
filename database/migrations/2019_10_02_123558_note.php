<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Note extends Migration
{
    /**
     * Lance la migration
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Notes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('description', 250);
            $table->date('date');
            $table->bigInteger('affilie_id')->unsigned();
            $table->foreign('affilie_id')->references('id')->on('Affilies');

        });
    }

    /**
     * Retire la migration
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Notes');
    }
}
