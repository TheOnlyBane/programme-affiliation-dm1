<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Affilie;
use App\Projet;
use Faker\Generator as Faker;

$factory->define(Projet::class, function (Faker $faker) {
    return [
        'type' => $faker->randomElement(array('recurrent', 'simple')),
        'nom'  => $faker->company,
        'date' => date($format = 'Y-m-d', $max = 'now'),
        'status' => $faker->randomElement(array('en attente', 'en cours')),
        'commission' => $faker->randomNumber(2),
        'montantTotal' => $faker->randomNumber(5),
        'affilie' => Affilie::all()->first()->id
    ];
});
