<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Affilie;
use Faker\Generator as Faker;

$factory->define(Affilie::class, function (Faker $faker) {
    return [
        'lienPartage' => uniqid(),
        'solde'=> $faker->randomNumber(4),
        'commissionReccurent'=>$faker->randomNumber(2),
        'commissionSimple'=>$faker->randomNumber(2),
        'numTPS'=>$faker->randomNumber(4)."-".$faker->randomNumber(4),
        'numTVQ'=>$faker->randomNumber(4)."-".$faker->randomNumber(4),
        'nomEntreprise'=>$faker->company,
        'numEntreprise'=>$faker->randomNumber(4)."-".$faker->randomNumber(4),
        'numTelEntreprise'=>$faker->phoneNumber
    ];
});
